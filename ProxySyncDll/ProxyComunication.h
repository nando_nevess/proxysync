#pragma once
#include <ios>
#include <iostream>
#include <Windows.h>
#include <stdio.h>
#include <stdint.h>
#include <string>

namespace params_t {
	typedef void(*message_callback)(wchar_t* message, uint32_t message_len);
	typedef void(*machine_mismatch_callback)();
}

namespace methods_t {
	typedef void(*set_message_callback)(params_t::message_callback _callback);
	typedef void(*set_machine_callback)(params_t::machine_mismatch_callback _callback);

	typedef void(*set_user_email)(wchar_t* user_email);
	typedef void(*set_software_id)(uint32_t software_id);

	typedef void(*hello_word)();
	typedef void(*set_user)(wchar_t* str);
	typedef void(*set_user_token)(void* data, uint32_t len);
	typedef void(*set_machine_id)(void* data, uint32_t len);

	typedef void(*run)(wchar_t* path_file);
}

class ProxyComunication {
	bool loaded = false;

	bool load() {
		HMODULE _module = LoadLibrary("ProxySyncDll.dll");
		if (!_module)
			return false;

		*(int*)&set_software_id = (int)GetProcAddress(_module, "set_software_id");
		if (!set_software_id)
			return false;

		*(int*)&set_user_email = (int)GetProcAddress(_module, "set_user_email");
		if (!set_user_email)
			return false;

		*(int*)&hello_word = (int)GetProcAddress(_module, "hello_word");
		if (!hello_word)
			return false;

		*(int*)&set_message_callback = (int)GetProcAddress(_module, "set_message_callback");
		if (!set_message_callback)
			return false;

		*(int*)&set_machine_callback = (int)GetProcAddress(_module, "set_machine_callback");
		if (!set_machine_callback)
			return false;

		*(int*)&set_user = (int)GetProcAddress(_module, "set_user");
		if (!set_user)
			return false;

		*(int*)&set_user_token = (int)GetProcAddress(_module, "set_user_token");
		if (!set_user_token)
			return false;

		*(int*)&set_machine_id = (int)GetProcAddress(_module, "set_machine_id");
		if (!set_machine_id)
			return false;

		*(int*)&run = (int)GetProcAddress(_module, "run");
		if (!run)
			return false;

		return true;
	}

	ProxyComunication() {
		loaded = load();
	}
public:

	methods_t::set_message_callback set_message_callback; 
	methods_t::set_machine_callback set_machine_callback;
	methods_t::set_user set_user;
	methods_t::hello_word hello_word;
	methods_t::set_user_token set_user_token;
	methods_t::set_machine_id set_machine_id;
	methods_t::set_software_id set_software_id;
	methods_t::set_user_email set_user_email;
	methods_t::run run;

	bool is_loaded() {
		return loaded;
	}

	static ProxyComunication* get() {
		static ProxyComunication* mproxy_comunication = nullptr;
		if (!mproxy_comunication)
			mproxy_comunication = new ProxyComunication();

		return mproxy_comunication;
	}
};