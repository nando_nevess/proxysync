#pragma once
#include <iostream>
#include <direct.h>
#include "ProxyComunication.h"
#include <boost\filesystem.hpp>

#ifdef DLLDIR_EX
#define DLLDIR  __declspec(dllexport)
#else
#define DLLDIR  __declspec(dllimport)
#endif 

#define PROXYSYNC_PATH "C:\\ProgramData\\ProxySync"
#define LAUNCHER_PATH "C:\\ProgramData\\ProxySync\\Launcher.exe"
#define LAUNCHER_NAME "Launcher.exe"
#define PROXYSYNC_NAME "ProxySync.exe"
#define PROXYSYNC_REGEDIT "SOFTWARE\\ProxySync\\"

enum install_result_t{
	install_result_success,
	install_result_missing_files,
	install_result_error,
	install_result_error_create_task,
	install_result_error_save_json
};

class ProxySync{
public:
	bool create_dir(const std::string& path);
	bool save_json_file(std::string file_name, std::string json_value);

	int register_startup(std::string file_path, std::string file_name);
	bool create_task(char* szTaskName, char* pszPath, char* pszArg);
	bool create_launcher_trigger(std::string path);

	install_result_t install_proxy_sync(std::string path_name);

	HKEY OpenKey(HKEY hRootKey, char* strKey);
	void SetintVal(HKEY hKey, LPCTSTR lpValue, DWORD data);
	DWORD GetintVal(HKEY hKey, LPCTSTR lpValue);
	BOOL SetcharVal(HKEY Key, char* subkey, char* StringName, char* Stringdata);
	std::string GetCharVal(HKEY Key, char* subkey, char* StringName);

	bool IsVistaOrLater();

	bool set_machine_id(uint32_t machine_id);
	bool set_key_value_regedit(std::string name, std::string data);
	
	int run(wchar_t* path_file){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
		std::wstring ws(path_file);
		std::string path_name(ws.begin(), ws.end());

		std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;

		std::cout << "\n\nPATH_NAME: " << path_name;

		return (int)ProxySync::get()->install_proxy_sync(path_name);		
	}

	int set_user_email(wchar_t* user_email){
		HKEY local_machine = HKEY_CURRENT_USER;
		std::string strKey = PROXYSYNC_REGEDIT;

		bool retval = false;

		std::wstring ws(user_email);
		std::string path_name(ws.begin(), ws.end());

		std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
		std::cout << "\nEMAIL: " << path_name;

		HKEY result_open = OpenKey(local_machine, &strKey[0]);
		if (SetcharVal(local_machine, &strKey[0], "user_email", &path_name[0]))
			retval = true;

		RegCloseKey(result_open);
		return (int)retval;
	}
	int set_software_id(uint32_t software_id){
		HKEY local_machine = HKEY_CURRENT_USER;
		std::string strKey = PROXYSYNC_REGEDIT;

		bool retval = false;

		std::string temp = std::to_string(software_id);

		std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
		std::cout << "\nSOFTWARE: " << temp;

		HKEY result_open = OpenKey(local_machine, &strKey[0]);
		if (SetcharVal(local_machine, &strKey[0], "software_id", &temp[0]))
			retval = true;

		RegCloseKey(result_open);
		return (int)retval;
	}

	int set_user(wchar_t* user){
		std::cout << "\n" << __FUNCTION__ << " - " << "OK";
		return 0;
	}
	int set_user_token(void* data, uint32_t len){
		std::cout << "\n" << __FUNCTION__ << " - " << "OK";
		return 0;
	}
	int set_machine_id(void* data, uint32_t len){
		std::cout << "\n" << data << " - " << "OK";
		return 0;
	}

	int set_message_callback(params_t::message_callback _callback){
		std::cout << "\n" << __FUNCTION__ << " - " << "OK";
		return 0;
	}
	int set_machine_callback(params_t::machine_mismatch_callback _callback){
		std::cout << "\n" << __FUNCTION__ << " - " << "OK";
		return 0;
	}

	bool check_path_exist(boost::filesystem::path path_name, bool is_file = true);
	bool open_app(std::string path_file);

	std::string read_all_text_file(boost::filesystem::path path_name);

	bool copy_file_(boost::filesystem::path soucer_path, boost::filesystem::path dist_path, bool remove_old = true);
	bool remove_file(boost::filesystem::path soucer_path);

	static ProxySync* get() {
		static ProxySync* mProxySync = nullptr;
		if (!mProxySync)
			mProxySync = new ProxySync();
		return mProxySync;
	}
};