#pragma once
#include "ProxySync.h"
#include "Json\json.h"

#include <boost\filesystem.hpp>

#include <locale>
#include <iomanip>
#include <cstdint>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <functional>

#include <conio.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>
#include <algorithm>
#include <regex>
#include <ShellAPI.h>
#include <windows.h>
#include <wininet.h>
#include <WinUser.h>
#include <time.h>
#include <comdef.h>
#include <Sddl.h>
#include <taskschd.h>
#include <tlhelp32.h>
#include <VersionHelpers.h>

#pragma comment(lib, "shell32.lib")
#pragma comment(lib, "taskschd.lib")
#pragma comment(lib, "comsupp.lib")
#pragma comment(lib, "version.lib" )
#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "Wininet.lib")
#pragma comment(lib, "User32.lib")
#pragma comment(lib, "shell32.lib")

bool ProxySync::check_path_exist(boost::filesystem::path path_name, bool is_file){
	if (!boost::filesystem::exists(path_name))
		return false;

	if (is_file){
		if (!boost::filesystem::is_regular_file(path_name))
			return false;
	}

	return true;
}
bool ProxySync::copy_file_(boost::filesystem::path soucer_path, boost::filesystem::path dist_path, bool remove_old){
	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (!check_path_exist(soucer_path))
		return false;

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (check_path_exist(dist_path)){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		boost::filesystem::remove(dist_path);
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	boost::filesystem::copy_file(soucer_path, dist_path);
	if (remove_old){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		if (check_path_exist(soucer_path))
			boost::filesystem::remove(soucer_path);
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	return true;
}
bool ProxySync::remove_file(boost::filesystem::path soucer_path){
	if (!check_path_exist(soucer_path))
		return false;

	boost::filesystem::remove(soucer_path);
}
bool ProxySync::IsVistaOrLater() {
	DWORD version = GetVersion();

	DWORD major = (DWORD)(LOBYTE(LOWORD(version)));
	DWORD minor = (DWORD)(HIBYTE(LOWORD(version)));

	return (major > 6) || ((major == 6) && (minor >= 0));

}

bool ProxySync::set_machine_id(uint32_t machine_id){
	HKEY local_machine = HKEY_CURRENT_USER;
	std::string strKey = PROXYSYNC_REGEDIT;
	std::string machine_id_string = std::to_string(machine_id);

	bool retval = false;

	HKEY result_open = OpenKey(local_machine, &strKey[0]);
	if (SetcharVal(local_machine, &strKey[0], "machine_id", &machine_id_string[0]))
		retval = true;

	RegCloseKey(result_open);
	return retval;
}
bool ProxySync::set_key_value_regedit(std::string name, std::string data){
	HKEY local_machine = HKEY_CURRENT_USER;
	std::string strKey = PROXYSYNC_REGEDIT;

	bool retval = false;

	HKEY result_open = OpenKey(local_machine, &strKey[0]);
	if (SetcharVal(local_machine, &strKey[0], &name[0], &data[0]))
		retval = true;

	RegCloseKey(result_open);
	return retval;
}

HKEY ProxySync::OpenKey(HKEY hRootKey, char* strKey){
	HKEY hKey;
	LONG nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS, &hKey);

	if (nError == ERROR_FILE_NOT_FOUND){
		nError = RegCreateKeyEx(hRootKey, strKey, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
		std::cout << "Creating registry key: " << strKey << std::endl;
		std::cout << "Error: " << nError << " Could not find or create " << strKey << std::endl;
	}

	if (nError)
		std::cout << "Error: " << nError << " Could not find or create " << strKey << std::endl;

	return hKey;
}

void ProxySync::SetintVal(HKEY hKey, LPCTSTR lpValue, DWORD data){
	LONG nError = RegSetValueEx(hKey, lpValue, NULL, REG_DWORD, (LPBYTE)&data, sizeof(DWORD));

	if (nError)
		std::cout << "Error: " << nError << " Could not set registry value: " << (char*)lpValue << std::endl;
}
DWORD ProxySync::GetintVal(HKEY hKey, LPCTSTR lpValue){
	DWORD data;
	DWORD size = sizeof(data);
	DWORD type = REG_DWORD;
	LONG nError = RegQueryValueEx(hKey, lpValue, NULL, &type, (LPBYTE)&data, &size);

	if (nError == ERROR_FILE_NOT_FOUND)
		data = 0;
	else if (nError)
		std::cout << "Error: " << nError << " Could not get registry value " << (char*)lpValue << std::endl;

	return data;
}

BOOL ProxySync::SetcharVal(HKEY Key, char* subkey, char* StringName, char* Stringdata){
	HKEY hKey = OpenKey(Key, subkey);

	LONG openRes = RegOpenKeyEx(Key, subkey, 0, KEY_ALL_ACCESS, &hKey);

	if (openRes == ERROR_SUCCESS) {
	}
	else
		printf("Error opening key.");

	LONG setRes = RegSetValueEx(hKey, StringName, 0, REG_SZ, (LPBYTE)Stringdata, strlen(Stringdata) + 1);
	if (setRes == ERROR_SUCCESS) {
	}
	else
		printf("Error writing to Registry.");

	LONG closeOut = RegCloseKey(hKey);
	if (closeOut == ERROR_SUCCESS) {
	}
	else
		printf("Error closing key.");

	return true;
}
std::string ProxySync::GetCharVal(HKEY Key, char* subkey, char* StringName){
	char value[1024];
	DWORD value_length = 1024;

	HKEY hKey = OpenKey(Key, subkey);
	LONG openRes = RegOpenKeyEx(Key, subkey, 0, KEY_ALL_ACCESS, &hKey);

	if (openRes == ERROR_SUCCESS) {
	}
	else
		printf("Error opening key.");

	LONG setRes = RegGetValue(Key, subkey, StringName, RRF_RT_ANY, NULL, (PVOID)&value, &value_length);
	if (setRes == ERROR_SUCCESS) {
	}
	else
		printf("Error writing to Registry.");

	LONG closeOut = RegCloseKey(hKey);
	if (closeOut == ERROR_SUCCESS) {
	}
	else
		printf("Error closing key.");

	return value;
}

install_result_t ProxySync::install_proxy_sync(std::string path_name){
	boost::filesystem::path path_launcher = path_name;

	if (boost::filesystem::is_regular_file(path_launcher))
		path_name = path_launcher.branch_path().string() + "\\Launcher.exe";
	else
		path_name = path_launcher.string() + "\\Launcher.exe";

	path_launcher = path_name;
	std::cout << "\n path_launcher: " << path_launcher;

	if (!check_path_exist(path_launcher, true))
		return install_result_missing_files;

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	std::string path_proxy = PROXYSYNC_PATH;
	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	if (!check_path_exist(path_proxy, false))
		create_dir(path_proxy);

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	std::string path_json_temp = path_proxy + "\\json_temp.json";
	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	if (check_path_exist(path_json_temp, true))
		boost::filesystem::remove(path_json_temp);

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	boost::filesystem::path current_path = path_launcher;
	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	if (!save_json_file(path_json_temp, current_path.branch_path().string()))
		return install_result_error_save_json;

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	boost::filesystem::path soucer_path_open = current_path.branch_path().string() + "\\Launcher.exe";
	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	boost::filesystem::path dist_path_open = path_proxy + "\\Launcher.exe";

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	if (!copy_file_(soucer_path_open, dist_path_open))
		return install_result_missing_files;

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	if (!create_launcher_trigger(PROXYSYNC_PATH))
		return install_result_error_create_task;

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
	if (!open_app(LAUNCHER_PATH))
		return install_result_missing_files;
}

bool ProxySync::save_json_file(std::string file_name, std::string json_value){
	std::ofstream jsonSet;

	jsonSet.open(file_name);
	if (!jsonSet.is_open())
		return false;

	jsonSet << json_value;
	jsonSet.close();
	return true;
}
bool ProxySync::create_dir(const std::string& path){
	if (check_path_exist(path))
		return false;
	else
		_mkdir(&path[0]);

	return true;
}
bool ProxySync::open_app(std::string path_file){
	if (!check_path_exist(path_file))
		return false;

	ShellExecute(NULL, "open", &path_file[0], "c:\\temp\\report.txt", NULL, SW_SHOWNORMAL);
	return true;
}
int ProxySync::register_startup(std::string file_path, std::string file_name){
	HKEY hKey;

	LONG lnRes = RegOpenKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", 0, KEY_WRITE, &hKey);
	if (lnRes == ERROR_SUCCESS)
		lnRes = RegSetValueEx(hKey, &file_name[0], 0, REG_SZ, (const BYTE*)&file_path[0], file_path.length());

	return 0;
}
bool ProxySync::create_task(char* szTaskName, char* pszPath, char* pszArg){
	std::cout << "\nszTaskName: " << szTaskName;
	std::cout << "\npszPath: " << pszPath;
	std::cout << "\npszArg: " << pszArg;

	BSTR bstrszTaskName = _com_util::ConvertStringToBSTR(szTaskName);
	BSTR bstrpszPath = _com_util::ConvertStringToBSTR(pszPath);
	BSTR bstrpszArg = _com_util::ConvertStringToBSTR(pszArg);

	ITaskService *pService;
	ITaskFolder *pRootFolder;
	ITaskDefinition *pTask;
	IRegistrationInfo *pRegInfo;
	IPrincipal *pPrincipal;
	ITaskSettings *pSettings;
	ITriggerCollection *pTriggerCollection;
	ITrigger *pTrigger;
	ILogonTrigger *pLogonTrigger;
	IActionCollection *pActionCollection;
	IAction *pAction;
	IExecAction *pExecAction;
	IRegisteredTask *pRegisteredTask;
	VARIANT var = { VT_EMPTY };

	bool triggersucc = false;
	bool actionsucc = false;
	bool succ = false;

	HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (FAILED(hr)){
		printf("\nCoInitializeEx failed: %x", hr);
		return 1;
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	hr = CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_PKT_PRIVACY, RPC_C_IMP_LEVEL_IMPERSONATE, NULL, 0, NULL);
	if (FAILED(hr)){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		CoUninitialize();

		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);

		return 1;
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (!SUCCEEDED(CoCreateInstance(CLSID_TaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskService, (void**)&pService))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);
		return 1;
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (!SUCCEEDED(pService->Connect(var, var, var, var))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		pService->Release();
		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);
		return 1;
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (!SUCCEEDED(pService->GetFolder(L"\\", &pRootFolder))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		pRootFolder->Release();
		pService->Release();
		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);
		return 1;
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (!SUCCEEDED(pService->NewTask(0, &pTask))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		pTask->Release();
		pRootFolder->Release();
		pService->Release();
		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);
		return 1;
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (SUCCEEDED(pTask->get_RegistrationInfo(&pRegInfo))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		pRegInfo->put_Author(L"AUTOHR");
		pRegInfo->Release();
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (SUCCEEDED(pTask->get_Principal(&pPrincipal))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		pPrincipal->put_RunLevel(TASK_RUNLEVEL_HIGHEST);
		printf("\nCoInitializeSecurity failed: %x", hr);
		pPrincipal->Release();
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (SUCCEEDED(pTask->get_Settings(&pSettings))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		pSettings->put_StartWhenAvailable(VARIANT_BOOL(true));
		pSettings->put_DisallowStartIfOnBatteries(VARIANT_BOOL(false));
		pSettings->put_StopIfGoingOnBatteries(VARIANT_BOOL(false));
		pSettings->put_ExecutionTimeLimit(TEXT(L"PT0S"));
		pSettings->Release();
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (SUCCEEDED(pTask->get_Triggers(&pTriggerCollection))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		printf("\nCoInitializeSecurity failed: %x", hr);
		if (SUCCEEDED(pTriggerCollection->Create(TASK_TRIGGER_LOGON, &pTrigger))){
			std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
			if (SUCCEEDED(pTrigger->QueryInterface(IID_ILogonTrigger, (void**)&pLogonTrigger))){
				std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
				if (SUCCEEDED(pLogonTrigger->put_Id(TEXT(L"AnyLogon"))) && SUCCEEDED(pLogonTrigger->put_UserId(NULL))){
					std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
					triggersucc = true;
				}
				pLogonTrigger->Release();
			}
			pTrigger->Release();
		}
		pTriggerCollection->Release();
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (SUCCEEDED(pTask->get_Actions(&pActionCollection))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		if (SUCCEEDED(pActionCollection->Create(TASK_ACTION_EXEC, &pAction))){
			std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
			if (SUCCEEDED(pAction->QueryInterface(IID_IExecAction, (void**)&pExecAction))){
				std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
				if (SUCCEEDED(pExecAction->put_Path(bstrpszPath)) && SUCCEEDED(pExecAction->put_Arguments(bstrpszArg))){
					std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
					actionsucc = true;
				}
				pExecAction->Release();
			}
			pAction->Release();
		}
		pActionCollection->Release();
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	if (triggersucc && actionsucc && SUCCEEDED(pRootFolder->RegisterTaskDefinition(bstrszTaskName, pTask, TASK_CREATE_OR_UPDATE, var, var, TASK_LOGON_INTERACTIVE_TOKEN, var, &pRegisteredTask))){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
		succ = true;
		pRegisteredTask->Release();
	}

	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	SysFreeString(bstrszTaskName);
	SysFreeString(bstrpszPath);
	SysFreeString(bstrpszArg);
	std::cout << "\nFUNCTION: " << __FUNCTION__ << " | LINE: " << __LINE__;
	return succ;
}
bool ProxySync::create_launcher_trigger(std::string path){
	boost::filesystem::path full_path = path;
	if (!IsVistaOrLater())
		return register_startup("\"" + full_path.string() + "\\Initializer.exe" + "\"", "TEST APP");
	else
		return create_task("NbTray", &("\"" + full_path.string() + "\\Initializer.exe" + "\"")[0], "");
}

extern "C" {
	int DLLDIR hello_word() {
		std::cout << "\n Hello World: ";
		return 0;
	}

	int DLLDIR run(wchar_t* path_file){
		std::cout << "\nFUNCTION: " << __FUNCTION__ << " || LINE: " << __LINE__;
		return ProxySync::get()->run(path_file);
	}

	int DLLDIR set_user(wchar_t* str){
		return ProxySync::get()->set_user(str);
	}

	int DLLDIR set_user_email(wchar_t* user_email){
		return ProxySync::get()->set_user_email(user_email);
	}
	int DLLDIR set_software_id(uint32_t software_id){
		return ProxySync::get()->set_software_id(software_id);
	}

	int DLLDIR set_user_token(void* data, uint32_t len){
		return ProxySync::get()->set_user_token(data,len);
	}
	int DLLDIR set_machine_id(void* data, uint32_t len){
		return ProxySync::get()->set_machine_id(data,len);
	}

	int DLLDIR set_message_callback(params_t::message_callback _callback){
		return ProxySync::get()->set_message_callback(_callback);
	}
	int DLLDIR set_machine_callback(params_t::machine_mismatch_callback _callback){
		return ProxySync::get()->set_machine_callback(_callback);
	}
}