#pragma once 
#include <iostream>
#include <ios>
#include <windows.h>
#include <stdio.h>
#include <atlstr.h>
#include <locale>
#include <codecvt>
#include <string>
#include <malloc.h>

#define TOTALBYTES    8192
#define BYTEINCREMENT 4096

#include "..\ProxySyncDll\ProxyComunication.h"

template <typename Facet>
struct deletable_facet : Facet
{
	using Facet::Facet;
};

#define PROXYSYNC_REGEDIT "SOFTWARE\\ProxySync"

HKEY OpenKey(HKEY hRootKey, char* strKey){
	HKEY hKey;
	LONG nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS, &hKey);

	if (nError == ERROR_FILE_NOT_FOUND){
		nError = RegCreateKeyEx(hRootKey, strKey, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
		std::cout << "Creating registry key: " << strKey << std::endl;
		std::cout << "Error: " << nError << " Could not find or create " << strKey << std::endl;
	}

	if (nError)
		std::cout << "Error: " << nError << " Could not find or create " << strKey << std::endl;

	return hKey;
}

void SetintVal(HKEY hKey, LPCTSTR lpValue, DWORD data){
	LONG nError = RegSetValueEx(hKey, lpValue, NULL, REG_DWORD, (LPBYTE)&data, sizeof(DWORD));

	if (nError)
		std::cout << "Error: " << nError << " Could not set registry value: " << (char*)lpValue << std::endl;
}
DWORD GetintVal(HKEY hKey, LPCTSTR lpValue){
	DWORD data;
	DWORD size = sizeof(data);
	DWORD type = REG_DWORD;
	LONG nError = RegQueryValueEx(hKey, lpValue, NULL, &type, (LPBYTE)&data, &size);

	if (nError == ERROR_FILE_NOT_FOUND)
		data = 0;
	else if (nError)
		std::cout << "Error: " << nError << " Could not get registry value " << (char*)lpValue << std::endl;

	return data;
}

BOOL SetcharVal(HKEY Key, char* subkey, char* StringName, char* Stringdata){
	HKEY hKey = OpenKey(Key, subkey);

	LONG openRes = RegOpenKeyEx(Key, subkey, 0, KEY_ALL_ACCESS, &hKey);

	if (openRes == ERROR_SUCCESS) {
	}
	else
		printf("Error opening key.");

	LONG setRes = RegSetValueEx(hKey, StringName, 0, REG_SZ, (LPBYTE)Stringdata, strlen(Stringdata) + 1);
	if (setRes == ERROR_SUCCESS) {
	}
	else
		printf("Error writing to Registry.");

	LONG closeOut = RegCloseKey(hKey);
	if (closeOut == ERROR_SUCCESS) {
	}
	else
		printf("Error closing key.");

	return true;
}
std::string GetCharVal(HKEY Key, char* subkey, char* StringName){
	char value[1024];
	DWORD value_length = 1024;

	HKEY hKey = OpenKey(Key, subkey);
	LONG openRes = RegOpenKeyEx(Key, subkey, 0, KEY_ALL_ACCESS, &hKey);

	if (openRes == ERROR_SUCCESS) {
	}
	else
		printf("Error opening key.");

	LONG setRes = RegGetValue(Key, subkey, StringName, RRF_RT_ANY, NULL, (PVOID)&value, &value_length);
	if (setRes == ERROR_SUCCESS) {
	}
	else{
		printf("Error writing to Registry.");
		return std::string("");
	}

	LONG closeOut = RegCloseKey(hKey);
	if (closeOut == ERROR_SUCCESS) {
	}
	else
		printf("Error closing key.");

	return std::string(value);
}

void test(){
	HKEY local_machine = HKEY_CURRENT_USER;
	std::string strKey = PROXYSYNC_REGEDIT;

	HKEY result_open = OpenKey(local_machine, &strKey[0]);
	SetcharVal(local_machine, &strKey[0], "machine_id", "0000");
}

void test2(){
	HKEY local_machine = HKEY_CURRENT_USER;
	std::string strKey = PROXYSYNC_REGEDIT;

	HKEY result_open = OpenKey(local_machine, &strKey[0]);
	SetcharVal(local_machine, &strKey[0], "user_email", "test@hotmail.com");
}

#define BUFFER 8192

/*void function(void *p) {
	int *ip = (int*)p;
	*ip = 9;
}
int main() {
	int p;
	function((void*)&p);
	printf("%d", p);
	return 0;
}*/

int function_test(wchar_t* str, uint32_t len){
	std::cout << "\n\nFUNCINOU O CALLBACK";
	return 0;
}
int function_test2(){
	std::cout << "\n\nFUNCTION MACHINE CALLBACK";
	return 0;
}

std::wstring s2ws(const std::string& str){
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
	return wstrTo;
}
std::wstring wstring_from_string(std::string str) {
	if (str.empty()) return std::wstring();
	int size_needed = MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
	std::wstring wstrTo(size_needed, 0);
	MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), &wstrTo[0], size_needed);
	return wstrTo;
}

wchar_t* string_to_wchar(std::string path_string){
	std::wstring wstring(path_string.begin(), path_string.end());
	return const_cast<wchar_t*>(wstring.c_str());
}

int main(){
	if (ProxyComunication::get()->is_loaded()){
		//VOCE VAI PASSAR COMO PARAMETRO O ARQUIVO DO LAUNCHER OU PASTA ONDE ESTA
		char path[MAX_PATH];
		HMODULE hModule = GetModuleHandleA(NULL);
		GetModuleFileNameA(hModule, path, MAX_PATH);

		std::string path_temp = std::string(path);
		std::wstring wstring(path_temp.begin(), path_temp.end());
		wchar_t* string = const_cast<wchar_t*>(wstring.c_str());
		
		//VOCE VAI PASSAR COMO PARAMETRO O ARQUIVO DO LAUNCHER OU PASTA ONDE ESTA
		ProxyComunication::get()->run(string);
	}

	std::cout << "\n\n";
	system("pause");

	return 0;
}

/*int main(){
/*char path[MAX_PATH];
HMODULE hModule = GetModuleHandleA(NULL);
GetModuleFileNameA(hModule, path, MAX_PATH);

ProxySync ProxySyncDLL;


RegGetValue(HKEY_CURRENT_USER, "SOFTWARE\\ProxySync\\", "user_email", RRF_RT_ANY, NULL, (PVOID)&value, &BufferSize);


HKEY local_machine = HKEY_CURRENT_USER;
std::string strKey = PROXYSYNC_REGEDIT;

const CString REG_SW_GROUP_I_WANT = _T("SOFTWARE\\ProxySync");
const CString REG_KEY_I_WANT = _T("user_email");

CRegKey regKey;
DWORD   dwValue = 0;

if (ERROR_SUCCESS != regKey.Open(HKEY_LOCAL_MACHINE, REG_SW_GROUP_I_WANT))
{
std::cout << (_T("CRegKey::Open failed in Method"));
regKey.Close();
}
if (ERROR_SUCCESS != regKey.QueryValue(dwValue, REG_KEY_I_WANT))
{
std::cout << (_T("CRegKey::QueryValue Failed in Method"));
regKey.Close();
}



auto temp = GetCharVal(local_machine, &strKey[0], "user_email");

std::cout << "\n TEMP: " << temp;


//install_result_t result = ProxySyncDLL.install_proxy_sync(path);

getchar();

return 0;
}*/