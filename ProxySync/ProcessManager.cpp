#include "ProcessManager.h"
#include "Utility.h"

BOOL CALLBACK enumWindowsProc(__in  HWND hWnd, __in  LPARAM lParam){
	int length = ::GetWindowTextLength(hWnd);
	if (0 == length) return TRUE;

	TCHAR* buffer;
	buffer = new TCHAR[length + 1];
	memset(buffer, 0, (length + 1) * sizeof(TCHAR));

	GetWindowText(hWnd, buffer, length + 1);
	std::string windowTitle = std::string(buffer);
	delete[] buffer;

	ProcessManager::get()->add_open_windows_list(std::string(windowTitle.begin(),windowTitle.end()));
	
	return TRUE;
}

bool ProcessManager::refresh_open_windows_list(){
	wcout << TEXT("Enumerating Windows...") << endl;
	
	this->clean_open_windows_list();
	BOOL enumeratingWindowsSucceeded = ::EnumWindows(enumWindowsProc, NULL);

	auto temp = this->get_open_windows_list();
	
	return 0;
}




