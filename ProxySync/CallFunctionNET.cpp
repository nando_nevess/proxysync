#pragma once
#include "CallFunctionNET.h"
#include <iostream>
#include <vector>

using namespace ProxySync;

void MessagePrinter::Print() {
	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	MainForm2^ mainForm = gcnew MainForm2();

	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	mainForm->ShowDialog();
}

void CallFunctionNET::InitializeMainForm(){
	/*std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	//MainForm2^ mainForm = gcnew MainForm2();

	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	//mainForm->ShowDialog();

	MessagePrinter^ printer1 = gcnew MessagePrinter();
	Thread^ thread1 = gcnew Thread(gcnew ThreadStart(printer1, &MessagePrinter::Print));
	thread1->SetApartmentState(System::Threading::ApartmentState::STA);
	thread1->Start();*/
}
void CallFunctionNET::ShowUpdateForm(String^ title, String^ message, cli::array<String^>^ array_images){
	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	MainForm^ mainForm = gcnew MainForm(title, message, array_images);
	
	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	mainForm->ShowDialog();
}
void CallFunctionNET::ShowBalloon(String^ title, String^ message, int timeout){
	NotifyIcon^ trayIcon = (gcnew System::Windows::Forms::NotifyIcon());

	trayIcon->Icon = System::Drawing::SystemIcons::Information;
	trayIcon->BalloonTipTitle = title;
	trayIcon->BalloonTipText = message;
	trayIcon->BalloonTipIcon = System::Windows::Forms::ToolTipIcon::Error;
	
	trayIcon->Visible = true;
	trayIcon->ShowBalloonTip(timeout);
}

void call_InitializeMainForm(){
	CallFunctionNET::Instance->InitializeMainForm();
}
void call_ShowUpdateForm(std::string title, std::string message, std::vector<std::string> vector_images){
	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	String^ title_ = gcnew String(title.c_str());

	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	String^ message_ = gcnew String(message.c_str());

	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	
	cli::array<String^>^ array_images = gcnew cli::array<String^>(vector_images.size());

	std::cout << "\nFUnction: " << __FUNCTION__ << " | line: " << __LINE__;
	int index = 0;
	std::cout << "\nFUnction: " << __FUNCTION__ << " | line: " << __LINE__;
	for (auto url : vector_images){
		std::cout << "\nFUnction: " << __FUNCTION__ << " | line: " << __LINE__;
		array_images[index] = gcnew String(url.c_str());
		std::cout << "\nFUnction: " << __FUNCTION__ << " | line: " << __LINE__;
		index++;
	}

	std::cout << "\nFUnction: " << __FUNCTION__ << " | line: " << __LINE__;
	CallFunctionNET::Instance->ShowUpdateForm(title_, message_, array_images);
}
void call_ShowBalloon(std::string title, std::string message, int timeout){
	String^ title_ = gcnew String(title.c_str());
	String^ message_ = gcnew String(message.c_str());

	CallFunctionNET::Instance->ShowBalloon(title_, message_, timeout);
}