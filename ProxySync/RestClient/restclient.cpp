#include "restclient.h"

#include <curl/curl.h>

#include "connection.h"

int RestClient::init() {
	CURLcode res = curl_global_init(CURL_GLOBAL_ALL);
	if (res == CURLE_OK) {
		return 0;
	}
	else {
		return 1;
	}
}

void RestClient::disable() {
	curl_global_cleanup();
}

RestClient::Response RestClient::get(const std::string& url) {
	RestClient::Response ret;
	RestClient::Connection *conn = new RestClient::Connection("");
	ret = conn->get(url);
	delete conn;
	return ret;
}

RestClient::Response RestClient::post(const std::string& url,const std::string& ctype,const std::string& data) {
	RestClient::Response ret;
	RestClient::Connection *conn = new RestClient::Connection("");
	conn->AppendHeader("Content-Type", ctype);
	ret = conn->post(url, data);
	delete conn;
	return ret;
}

RestClient::Response RestClient::put(const std::string& url,const std::string& ctype,const std::string& data) {
	RestClient::Response ret;
	RestClient::Connection *conn = new RestClient::Connection("");
	conn->AppendHeader("Content-Type", ctype);
	ret = conn->put(url, data);
	delete conn;
	return ret;
}

RestClient::Response RestClient::del(const std::string& url) {
	RestClient::Response ret;
	RestClient::Connection *conn = new RestClient::Connection("");
	ret = conn->del(url);
	delete conn;
	return ret;
}

RestClient::Response RestClient::head(const std::string& url) {
	RestClient::Response ret;
	RestClient::Connection *conn = new RestClient::Connection("");
	ret = conn->head(url);
	delete conn;
	return ret;
}
