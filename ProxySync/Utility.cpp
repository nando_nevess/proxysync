#pragma once
#include "Utility.h"

namespace Tools{
	HKEY OpenKey(HKEY hRootKey, char* strKey){
		HKEY hKey;
		LONG nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS, &hKey);

		if (nError == ERROR_FILE_NOT_FOUND){
			nError = RegCreateKeyEx(hRootKey, strKey, NULL, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
			std::cout << "Creating registry key: " << strKey << std::endl;
			std::cout << "Error: " << nError << " Could not find or create " << strKey << std::endl;
		}

		if (nError)
			std::cout << "Error: " << nError << " Could not find or create " << strKey << std::endl;

		return hKey;
	}

	void SetintVal(HKEY hKey, LPCTSTR lpValue, DWORD data){
		LONG nError = RegSetValueEx(hKey, lpValue, NULL, REG_DWORD, (LPBYTE)&data, sizeof(DWORD));

		if (nError)
			std::cout << "Error: " << nError << " Could not set registry value: " << (char*)lpValue << std::endl;
	}
	DWORD GetintVal(HKEY hKey, LPCTSTR lpValue){
		DWORD data;
		DWORD size = sizeof(data);
		DWORD type = REG_DWORD;
		LONG nError = RegQueryValueEx(hKey, lpValue, NULL, &type, (LPBYTE)&data, &size);

		if (nError == ERROR_FILE_NOT_FOUND)
			data = 0;
		else if (nError)
			std::cout << "Error: " << nError << " Could not get registry value " << (char*)lpValue << std::endl;

		return data;
	}

	BOOL SetcharVal(HKEY Key, char* subkey, char* StringName, char* Stringdata){
		HKEY hKey = OpenKey(Key, subkey);

		LONG openRes = RegOpenKeyEx(Key, subkey, 0, KEY_ALL_ACCESS, &hKey);

		if (openRes == ERROR_SUCCESS) {
		}
		else
			printf("Error opening key.");

		LONG setRes = RegSetValueEx(hKey, StringName, 0, REG_SZ, (LPBYTE)Stringdata, strlen(Stringdata) + 1);
		if (setRes == ERROR_SUCCESS) {
		}
		else
			printf("Error writing to Registry.");

		LONG closeOut = RegCloseKey(hKey);
		if (closeOut == ERROR_SUCCESS) {
		}
		else
			printf("Error closing key.");

		return true;
	}
	std::string GetCharVal(HKEY Key, char* subkey, char* StringName){
		char value[1024];
		DWORD value_length = 1024;

		HKEY hKey = OpenKey(Key, subkey);
		LONG openRes = RegOpenKeyEx(Key, subkey, 0, KEY_ALL_ACCESS, &hKey);

		if (openRes == ERROR_SUCCESS) {
		}
		else{
			printf("Error opening key.");
			return std::string("");
		}

		LONG setRes = RegGetValue(Key, subkey, StringName, RRF_RT_ANY, NULL, (PVOID)&value, &value_length);
		if (setRes == ERROR_SUCCESS) {
		}
		else{
			printf("Error writing to Registry.");
			return std::string("");
		}

		LONG closeOut = RegCloseKey(hKey);
		if (closeOut == ERROR_SUCCESS) {
		}
		else{
			printf("Error closing key.");
			return std::string("");
		}

		return std::string(value);
	}

	uint32_t GetLastInputTime(void)	{
		LASTINPUTINFO lastInput;
		lastInput.cbSize = sizeof(LASTINPUTINFO);

		BOOL success = GetLastInputInfo(&lastInput);
		if (!success)
			DWORD err = GetLastError();

		DWORD lastInputTime = lastInput.dwTime;
		DWORD currentTime = GetTickCount();

		DWORD timeElapsed = currentTime - lastInputTime;

		return (uint32_t)timeElapsed;
	}

	bool check_path_exist(boost::filesystem::path path_name){
		if (!boost::filesystem::exists(path_name))
			return false;

		return true;
	}

	bool IsVistaOrLater() {
		/*DWORD version = GetVersion();

		DWORD major = (DWORD)(LOBYTE(LOWORD(version)));
		DWORD minor = (DWORD)(HIBYTE(LOWORD(version)));

		return (major > 6) || ((major == 6) && (minor >= 0));*/
		return true;
	}

	BOOL SetMessageDuration(DWORD seconds, UINT flags){
		return SystemParametersInfo(0x2017, 0, IntToPtr(seconds), flags);
	}
	void notify_ballon(char*title, char* message, int timeout,int id){
		if (IsVistaOrLater())
			SetMessageDuration(timeout, 0);
		
		NOTIFYICONDATA nid;
		memset(&nid, 0, sizeof(NOTIFYICONDATA));
		nid.cbSize = sizeof(NOTIFYICONDATA);
		nid.hWnd = 0;
		nid.uID = id;
		nid.uFlags = NIF_INFO;
		//memcpy(nid.szInfo, message, 50);

		for (int i = 0; i < 255; i++){
			nid.szInfo[i] = message[i];
			if (!message[i])
				break;
		}

		for (int i = 0; i < 63; i++){
			nid.szInfoTitle[i] = title[i];
			if (!title[i])
				break;
		}


		nid.dwInfoFlags = NIIF_INFO;
		nid.uTimeout = timeout;

		Shell_NotifyIcon(NIM_DELETE, &nid);
		Shell_NotifyIcon(NIM_ADD, &nid);
	}
	bool check_json_is_empty(std::string json_string){
		json_string = remove_new_line(json_string);
		json_string = remove_space(json_string);

		if (json_string == "")
			return true;

		if (json_string == "[]")
			return true;

		return false;
	}

	std::string remove_barra(std::string in){
		std::string _temp = in;
		_temp.erase(std::remove(_temp.begin(), _temp.end(), '\\'), _temp.end());
		return _temp;
	}
	std::string remove_new_line(std::string in){
		std::string _temp = in;
		_temp.erase(std::remove(_temp.begin(), _temp.end(), '\n'), _temp.end());
		return _temp;
	}
	std::string remove_space(std::string in){
		std::string temp_ = in;
		temp_ = std::regex_replace(temp_, std::regex(" "), "");
		return temp_;
	}
	std::string replace(std::string text, std::string old_, std::string new_){
		std::string temp_ = text;
		temp_ = std::regex_replace(temp_, std::regex(old_), new_);
		return temp_;
	}

	Json::Value string_to_json(std::string json_data){
		Json::Value root;
		Json::Reader reader;
		bool parsingSuccessful = reader.parse(json_data.c_str(), root);
		if (!parsingSuccessful){
			std::cout << "\nFailed to parse" << reader.getFormattedErrorMessages();
			return 0;
		}	

		return root;
	}
	std::string json_to_string(Json::Value json_string){
		return json_string.toStyledString();
	}

	bool check_have_string(std::vector<std::string> word_to_find, std::string text) {
		for (auto word : word_to_find) {
			if (text.find(word) != string::npos)
				return true;
		}

		return false;
	}

	uint32_t string_to_uint32(std::string string_value){
		uint32_t retval = 0;
		try{
			retval = boost::lexical_cast<uint32_t>(string_value);
		}
		catch (...){
		}
		return retval;
	}

	std::time_t string_to_time_t(std::string current_time,std::string formt){
		struct std::tm tm;
		std::istringstream ss(current_time);
		ss >> std::get_time(&tm, &formt[0] /*"%d-%m-%Y %I:%M:%S"*/); // or just %T in this case
		std::time_t time = mktime(&tm);
		return time;
	}

	std::string wchar_to_string(WCHAR* string_to_cast) {
		std::wstring s(string_to_cast);
		std::string str(s.begin(), s.end());

		return str;
	}
	
	std::string get_current_time(){
		time_t rawtime;
		struct tm * timeinfo;
		char buffer[80];

		time(&rawtime);
		timeinfo = localtime(&rawtime);

		strftime(buffer, sizeof(buffer), "%d-%m-%Y_%I:%M:%S", timeinfo);
		std::string str(buffer);

		return str;
	}
	std::string string_trim(const std::string& str) {
		size_t first = str.find_first_not_of(' ');
		if (std::string::npos == first)
			return str;

		size_t last = str.find_last_not_of(' ');
		return str.substr(first, (last - first + 1));
	}

	std::string &lower(std::string &s){
		std::transform(s.begin(), s.end(), s.begin(), ::tolower);
		return s;
	}
}