#pragma once

class ProxySyncManager{
public:
	ProxySyncManager();

	void call_send_insert_click_count();

	static ProxySyncManager* get(){
		static ProxySyncManager* m = nullptr;
		if (!m)
			m = new ProxySyncManager();
		return m;
	}
};