#pragma once
#include "Utility.h"

struct ActionInfo{
	uint32_t execute_id = 0;
	uint32_t action_id = 0;
	std::string action_name = "";
	Json::Value action_args = "";
	uint32_t minutes_offline = 0;
	language_t language = (language_t)0;
	std::string proxy_send = "";
};

struct ActionInfoResult{
	uint32_t execute_id = 0;
	uint32_t action_id = 0;
	std::string action_name = "";
	std::string action_result = "";
};

struct ProcessInfoFromSQL{
	uint32_t process_id = 0;
	std::string process_name = "";
	bool process_register = false;
};

struct ProcessInfoToSQL{
	std::string process_name = "";
	std::string current_date = "";
	bool process_is_open = false;
};

class UserInfoManager{
	bool first_time = true;

	std::string current_proxy_used = "";
	uint32_t user_id = 0;
	uint32_t current_execute_id = 0;
	uint32_t current_execute_id_form = 0;

	language_t system_language = language_t::language_english;
	windows_version_t windows_version = windows_version_t::windows_version_none;

	std::map<uint32_t, std::shared_ptr<ActionInfo>> execute_action_list;
	std::vector<std::shared_ptr<ActionInfoResult>> execute_action_result_list;
	std::vector<std::shared_ptr<ProcessInfoFromSQL>> list_process_info_from_sql;
	std::vector<std::shared_ptr<ProcessInfoToSQL>> list_process_info_to_sql;

public:

	void add_list_process_info_to_sql(std::shared_ptr<ProcessInfoToSQL> process_info){
		for (auto curren_process_info : list_process_info_to_sql){
			if (curren_process_info->process_name != process_info->process_name)
				continue;	
		
			curren_process_info->process_is_open = process_info->process_is_open;
			curren_process_info->current_date = process_info->current_date;
			return;
		}

		list_process_info_to_sql.push_back(process_info);
	}
	std::vector<std::shared_ptr<ProcessInfoToSQL>> get_list_process_info_to_sql(){
		return this->list_process_info_to_sql;
	}

	void add_list_process_info_from_sql(std::shared_ptr<ProcessInfoFromSQL> process_info){
		list_process_info_from_sql.push_back(process_info);
	}
	std::vector<std::shared_ptr<ProcessInfoFromSQL>> get_list_process_info_from_sql(){
		return this->list_process_info_from_sql;
	}

	void set_current_proxy_used(std::string in){
		this->current_proxy_used = in;
	}
	std::string get_current_proxy_used(){
		return this->current_proxy_used;
	}
	
	std::string parse_process_info_to_json(){
		Json::Value json_retval;

		for (auto process_info : this->list_process_info_to_sql){
			if (!process_info)
				continue;

			Json::Value json_process_info;
			json_process_info["current_date"] = process_info->current_date;
			json_process_info["process_is_open"] = process_info->process_is_open;
			json_process_info["process_name"] = process_info->process_name;

			json_retval.append(json_process_info);
		}

		return json_retval.toStyledString();
	}

	void set_current_execute_id_form(uint32_t in){
		this->current_execute_id_form = in;
	}
	uint32_t get_current_execute_id_form(){
		return this->current_execute_id_form;
	}
	bool parse_json_to_execute_list(Json::Value& json_data);

	uint32_t get_time_idle(){
		return Tools::GetLastInputTime();
	}
	Json::Value execute_action_result_list_to_json(){
		Json::Value json_retval;

		for (auto action_result : execute_action_result_list){
			if (!action_result)
				continue;

			Json::Value json_result;
			json_result["execute_id"] = action_result->execute_id;
			json_result["action_id"] = action_result->action_id;
			json_result["action_name"] = action_result->action_name;
			json_result["action_result"] = action_result->action_result;

			json_retval.append(json_result);
		}

		return json_retval;
	}
	void add_execute_action_result_list(std::shared_ptr<ActionInfoResult> recent){
		auto temp = std::find(execute_action_result_list.begin(), execute_action_result_list.end(), recent);
		if (temp != execute_action_result_list.end())
			return;

		if (execute_action_result_list.size() < 4)
			execute_action_result_list.push_back(recent);
		else{
			execute_action_result_list.push_back(recent);
			execute_action_result_list.erase(execute_action_result_list.begin());
		}	
	}
	
	std::string get_proxy_dir();
	std::string get_proxy_used();

	std::string get_user_email_regedit();

	void update_current_execute_id();
	std::shared_ptr<ActionInfo> get_execute_action_info(uint32_t current_id);
	void clean_execute_action_list();
	uint32_t get_current_execute_id();
	void set_current_execute_id(uint32_t in);
	std::map<uint32_t, std::shared_ptr<ActionInfo>> get_execute_action_list();
	void set_system_language(language_t in);
	void set_windows_version(windows_version_t in);
	std::string language_t_string(language_t in);
	std::string windows_version_string(windows_version_t in);
	method_t string_to_method(std::string in);
	language_t string_to_language(std::string in);
	windows_version_t string_to_windows_version(std::string in);
	void update_infos();
	language_t get_system_language();
	windows_version_t get_windows_version();
	uint32_t get_user_id();
	uint32_t generate_user_id();
	void set_first_time(bool in);
	void parse_json_to_class(Json::Value json_value);
	Json::Value parse_class_to_json();
	
	void parse_json_to_process_info(Json::Value json_value);
	void parse_json_to_user_process_info(Json::Value json_value);
	
	static UserInfoManager* get(){
		static UserInfoManager* m = nullptr;
		if (!m)
			m = new UserInfoManager();
		return m;
	}
};