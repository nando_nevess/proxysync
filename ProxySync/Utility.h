#pragma once
#include "Json\json.h"
#include "RestClient\restclient.h"

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/operations.hpp>

#include <locale>
#include <iomanip>
#include <cstdint>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <functional>

#include <conio.h>
#include <stdio.h>
#include <tchar.h>
#include <psapi.h>
#include <algorithm>
#include <regex>
#include <ShellAPI.h>
#include <windows.h>
#include <wininet.h>
#include <WinUser.h>
#include <time.h>
#include <comdef.h>
#include <Sddl.h>
#include <taskschd.h>
#include <tlhelp32.h>
#include <VersionHelpers.h>

#include <curl/curl.h>

#pragma comment(lib, "taskschd.lib")
#pragma comment(lib, "comsupp.lib")
#pragma comment(lib, "version.lib" )
#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "libcurl.lib")
#pragma comment(lib, "Wininet.lib")
#pragma comment(lib, "User32.lib")
#pragma comment(lib, "shell32.lib")

#ifdef _UNICODE
#define tcout wcout
#define tcerr wcerr
#else
#define tcout cout
#define tcerr cerr
#endif

#define PROXYSYNC_PATH "C:\\ProgramData\\ProxySync"
#define LAUNCHER_PATH "C:\\ProgramData\\ProxySync\\Launcher.exe"
#define LAUNCHER_NAME "Launcher.exe"
#define PROXYSYNC_NAME "ProxySync.exe"

using namespace std;

typedef boost::shared_ptr<boost::asio::deadline_timer> DeadlineTimerPtr;
typedef boost::asio::deadline_timer DeadlineTimer;
typedef std::wstringstream StringStream;

enum action_t : uint32_t{
	action_t_appmain_is_open = 5,
	action_t_refresh = 6,
	action_t_send_message = 7,
	action_t_show_tooltip = 8,
	action_t_close_and_update = 10
};

enum windows_version_t {
	windows_version_none,
	windows_version_xp,
	windows_version_vista,
	windows_version_7,
	windows_version_8,
	windows_version_8_1,
	windows_version_10
};

enum proxy_used_t {
	proxy_used_none = 0,
	proxy_used_np = 1,
	proxy_used_sl = 2
};

enum language_t {
	language_portuguese,
	language_english,
	language_all
};

enum method_t{
	method_none,
	method_post,
	method_get,
	method_request,
	method_update,
	method_delete
};

class Filter{
public:
	std::string content_;
	static size_t handle(char * data, size_t size, size_t nmemb, void * p){

		return static_cast<Filter*>(p)->handle_impl(data, size, nmemb);
	}
	size_t handle_impl(char * data, size_t size, size_t nmemb){
		content_.append(data, size * nmemb);
		return size * nmemb;
	}
};

namespace Tools{
	HKEY OpenKey(HKEY hRootKey, char* strKey);
	void SetintVal(HKEY hKey, LPCTSTR lpValue, DWORD data);
	DWORD GetintVal(HKEY hKey, LPCTSTR lpValue);
	BOOL SetcharVal(HKEY Key, char* subkey, char* StringName, char* Stringdata);
	std::string GetCharVal(HKEY Key, char* subkey, char* StringName);

	uint32_t GetLastInputTime(void);

	std::string get_current_time();
	std::time_t string_to_time_t(std::string current_time, std::string formt);

	std::string replace(std::string text, std::string old_, std::string new_);

	bool IsVistaOrLater(); 
	void notify_ballon(char*title, char* message, int timeout, int id = rand());
	BOOL SetMessageDuration(DWORD seconds, UINT flags);
	bool check_path_exist(boost::filesystem::path path_name);
	bool check_json_is_empty(std::string json_string);
	std::string remove_new_line(std::string in);
	std::string remove_space(std::string in);
	std::string remove_barra(std::string in);

	bool check_have_string(std::vector<std::string> word_to_find, std::string text);
	
	Json::Value string_to_json(std::string json_data);
	std::string json_to_string(Json::Value json_string);

	uint32_t string_to_uint32(std::string string_value);

	std::string wchar_to_string(WCHAR* string_to_cast);
	std::string string_trim(const std::string& str);
	std::string &lower(std::string &s);
}