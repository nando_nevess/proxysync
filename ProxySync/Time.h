#pragma once

#include <windows.h>
#include <chrono>
#include <functional>

class Time{
public:
	static int get_current_time(int type);
};

class TimeChronometer{
	std::chrono::system_clock::time_point start_time;

public:
	TimeChronometer();
	void reset();
	int32_t elapsed_milliseconds();
	int32_t elapsed_microseconds();
	int32_t elapsed_nanoseconds();
	int32_t elapsed_seconds();
	void reset_time_with_timeout(int32_t temp = 2000){
		start_time = std::chrono::high_resolution_clock::now() + std::chrono::milliseconds(5000);
	}
	static bool wait_condition(std::function<bool()> condition, uint32_t timeout = 1000, uint32_t callback_check_delay = 10);

	void wait_complete_delay(uint32_t timeout);

	static TimeChronometer* get();
};


class FunctionCallbackAuto{
	std::function<void()> callback;
public:
	FunctionCallbackAuto(std::function<void()> callback_ptr){
		callback = callback_ptr;
	}

	~FunctionCallbackAuto(){
		if (callback)
			callback();
	}
};


