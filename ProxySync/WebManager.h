#pragma once
#include "Utility.h"
#include "FileManager.h"

class WebManager{
public:
	bool refresh_request_action(){
		uint32_t current_execute_id = UserInfoManager::get()->get_current_execute_id();
		std::string json_string = get_execute_actions(current_execute_id);
		Json::Value& json_data = Tools::string_to_json(json_string);

		return UserInfoManager::get()->parse_json_to_execute_list(json_data);
	}

	std::string get_execute_actions(uint32_t current_action_id){
		std::string url_end = "158.69.198.182:8080/actions/execute";
		url_end += "/" + std::to_string(current_action_id);

		std::cout << "\n\nUrl: " << url_end;

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		return response_body;
	}
	std::string get_process_info(){
		std::string url_end = "158.69.198.182:8080/process_info";

		std::cout << "\n\nUrl: " << url_end;

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;

		return response_body;
	}
	std::string get_user_process_info(uint32_t current_user_id){
		std::string url_end = "158.69.198.182:8080/process_info";
		url_end += "/" + std::to_string(current_user_id);

		std::cout << "\n\nUrl: " << url_end;

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;

		return response_body;
	}

	std::string send_infos(std::string data_json){
		uint32_t language = (uint32_t)UserInfoManager::get()->get_system_language();

		std::string proxy_dir = UserInfoManager::get()->get_proxy_dir();
		std::string proxy_used = UserInfoManager::get()->get_proxy_used();
				
		proxy_used = Tools::replace(proxy_used, " ", "_");

		proxy_dir = Tools::replace(proxy_dir, "\\\\", "-");
		proxy_dir = Tools::replace(proxy_dir, " ", "_");

		proxy_dir = (proxy_dir == "") ? "NULL" : proxy_dir;
		proxy_used = (proxy_used == "") ? "NULL" : proxy_used;

		if (proxy_used != "NULL" && proxy_used != "null")
			UserInfoManager::get()->set_current_proxy_used(proxy_used);

		std::string url_end = "158.69.198.182:8080/update";

		url_end += "/" + std::to_string(UserInfoManager::get()->get_user_id());
		url_end += "/" + data_json;
		url_end += "/" + std::to_string(UserInfoManager::get()->get_current_execute_id());
		url_end += "/" + std::to_string(language);
		url_end += "/" + proxy_dir;
		url_end += "/" + proxy_used;

		std::cout << "\n\nUrl: " << url_end;
		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		RestClient::Response r = RestClient::post(url_end, "text/json", "");
		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		std::string response_body = r.body;

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		return response_body;
	}
	std::string insert_click_count(uint32_t current_execute_id){
		std::string url_end = "158.69.198.182:8080/insert";
		url_end += "/" + std::to_string(current_execute_id);

		RestClient::Response response = RestClient::post(url_end, "text/json", "");
		std::string response_body = response.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		return response_body;
	}

	std::string get_last_time_proxy(uint32_t current_user_id){
		std::string url_end = "158.69.198.182:8080/get/last_time_proxy";
		url_end += "/" + std::to_string(current_user_id);

		std::cout << "\n\nUrl: " << url_end;

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		return response_body;
	}
	std::string send_last_time_proxy(uint32_t current_user_id){
		std::string url_end = "158.69.198.182:8080/update/last_time_proxy";

		std::string proxy_used = UserInfoManager::get()->get_proxy_used();
		proxy_used = Tools::replace(proxy_used, " ", "_");
		proxy_used = (proxy_used == "") ? "NULL" : proxy_used;

		if (proxy_used != "NULL" && proxy_used != "null")
			UserInfoManager::get()->set_current_proxy_used(proxy_used);

		url_end += "/" + std::to_string(current_user_id);

		std::cout << "\n\nUrl: " << url_end;

		RestClient::Response r = RestClient::post(url_end, "text/json", "");
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		return response_body;
	}
	std::string send_process_infos(uint32_t current_user_id, std::string data_json){
		std::string url_end = "158.69.198.182:8080/process_info/update";

		url_end += "/" + std::to_string(current_user_id);
		url_end += "/" + data_json;

		std::cout << "\n\nUrl: " << url_end;

		RestClient::Response r = RestClient::post(url_end, "text/json", "");
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		return response_body;
	}
	std::string send_user_email(uint32_t current_user_id, std::string user_email){
		std::string url_end = "158.69.198.182:8080/update/email";

		url_end += "/" + std::to_string(current_user_id);
		user_email = Tools::replace(user_email, "[.]", "-");
		url_end += "/" + user_email;

		std::cout << "\n\nUrl: " << url_end;

		RestClient::Response r = RestClient::post(url_end, "text/json", "");
		std::string response_body = r.body;

		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY START - - - - - - - - - - - - - - - - - - - - \n\n" << response_body;
		std::cout << "\n\n - - - - - - - - - - - - - - - - - - - - BODY END- - - - - - - - - - - - - - - - - - - - - -";

		return response_body;
	}
	
	uint32_t get_last_execute_id(){
		std::string url_end = "158.69.198.182:8080/actions/execute";

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		Json::Value& json_data = Tools::string_to_json(response_body);
		if (json_data.empty())
			return 0;

		if (json_data.isNull())
			return 0;

		uint32_t _current_user_id = 0;
		for (auto member : json_data){
			if (member.empty())
				continue;

			if (member.isNull())
				continue;

			std::string action_id = member["execute_id"].asString();
			_current_user_id = Tools::string_to_uint32(action_id);
		}

		return _current_user_id;
	}
	std::string get_proxy_used(uint32_t user_id){
		std::string url_end = "158.69.198.182:8080/user_info/" + std::to_string(user_id);

		RestClient::Response r = RestClient::get(url_end);
		std::string response_body = r.body;

		Json::Value json_data = Tools::string_to_json(response_body);
		if (json_data.empty())
			return "null";

		if (json_data.isNull())
			return "null";

		std::string _current_user_id = "";
		for (auto member : json_data){
			if (member.empty())
				continue;

			if (member.isNull())
				continue;

			_current_user_id = member["proxy_used"].asString();
		}
		return _current_user_id;
	}

	bool check_connection(){
		if (InternetCheckConnection("http://www.google.com", FLAG_ICC_FORCE_CONNECTION, 0))
			return true;

		return false;
	}

	static WebManager* get(){
		static WebManager* m = nullptr;
		if (!m)
			m = new WebManager();
		return m;
	}
};