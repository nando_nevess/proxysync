#pragma once
#include "UserInfoManager.h"

struct FormInfo{
	action_t action = (action_t)0;
	Json::Value json_args = Json::Value();
	std::string result = "";
	bool is_running = false;
};

class SoftwareManager{
	uint32_t* mtx_uint;

	bool is_runnings = false;
	bool active_thread_form = false;
public:
	std::shared_ptr<FormInfo> current_show_form;

	SoftwareManager();
	~SoftwareManager();
		
	std::shared_ptr<ActionInfoResult> generate_action_info_result(uint32_t current_execute_action_id);

	void wait_idle_timeout(uint32_t timeout){
		while (true){
			int idle_time = UserInfoManager::get()->get_time_idle();
			if (idle_time > timeout)
				return;

			Sleep(2 * 1000);
		}
	}

	void init_thread();
	void start_loop();
	void thread_execute_actions();
	void thread_process_refresh();

	void show_form_update(std::string title, std::string message, std::vector<std::string> vector_images);

	std::string execute_action_t(action_t action, Json::Value json_args);

	static SoftwareManager* get(){
		static SoftwareManager* m = nullptr;
		if (!m)
			m = new SoftwareManager();
		return m;
	}
};