#pragma once
#include "UserInfoManager.h"
#include "ProcessManager.h"

bool UserInfoManager::parse_json_to_execute_list(Json::Value& json_data){
	if (json_data.empty() || json_data.isNull())
		return false;
	
	execute_action_list.clear();
	for (auto member : json_data){
		std::shared_ptr<ActionInfo> new_action_info = std::make_shared<ActionInfo>();

		std::string action_id = member["action_id"].asString();
		new_action_info->action_id = Tools::string_to_uint32(action_id);

		std::string execute_id = member["execute_id"].asString();
		new_action_info->execute_id = Tools::string_to_uint32(execute_id);

		std::string action_name = member["action_name"].asString();
		new_action_info->action_name = action_name;

		Json::Value language_json = member["language"];
		if (!language_json.isNull() && !language_json.empty()){
			std::string language_string = language_json.asString();
			new_action_info->language = (language_t)Tools::string_to_uint32(language_string);
		}

		Json::Value proxy_send_json = member["proxy_send"];
		if (!proxy_send_json.isNull() && !proxy_send_json.empty()){
			std::string proxy_send_string = proxy_send_json.asString();
			new_action_info->proxy_send = proxy_send_string;
		}

		Json::Value action_args = member["action_args"];
		new_action_info->action_args = action_args;

		std::string minutes_offline_string = member["minutes_offline"].asString();
		new_action_info->minutes_offline = Tools::string_to_uint32(minutes_offline_string);

		uint32_t temp_execute_id = Tools::string_to_uint32(execute_id);
		if (this->execute_action_list.find(temp_execute_id) == execute_action_list.end())
			execute_action_list.insert(std::make_pair(temp_execute_id, new_action_info));
	}

	return true;
}
void UserInfoManager::update_current_execute_id(){
	this->current_execute_id += 1;
}
std::shared_ptr<ActionInfo> UserInfoManager::get_execute_action_info(uint32_t current_id){
	std::shared_ptr<ActionInfo> retval;

	auto execute_action_list = UserInfoManager::get()->get_execute_action_list();
	for (auto current_action : execute_action_list){
		if (!current_action.second)
			continue;

		if (current_action.first < current_id)
			continue;

		UserInfoManager::get()->set_current_execute_id(current_action.first);
		retval = current_action.second;
		break;
	}

	return retval;
}
void UserInfoManager::clean_execute_action_list(){
	uint32_t current_id = this->current_execute_id;

	for (auto current_action = execute_action_list.begin(); current_action != execute_action_list.end();){
		if (current_action->first >= current_execute_id)
			current_action = execute_action_list.erase(current_action);
		else
			current_action++;
	}
}
uint32_t UserInfoManager::get_current_execute_id(){
	return current_execute_id;
}
void UserInfoManager::set_current_execute_id(uint32_t in){
	this->current_execute_id = in;
}
std::map<uint32_t, std::shared_ptr<ActionInfo>> UserInfoManager::get_execute_action_list(){
	return this->execute_action_list;
}
void UserInfoManager::set_system_language(language_t in){
	this->system_language = in;
}
void UserInfoManager::set_windows_version(windows_version_t in){
	this->windows_version = in;
}
std::string UserInfoManager::language_t_string(language_t in){
	switch (in){
	case language_portuguese:
		return "language_portuguese";
		break;
	case language_english:
		return "language_english";
		break;
	default:
		return "language_none";
		break;
	}
}
std::string UserInfoManager::windows_version_string(windows_version_t in){
	switch (in)
	{
	case windows_version_none:
		return "windows_version_none";
		break;
	case windows_version_xp:
		return "windows_version_xp";
		break;
	case windows_version_vista:
		return "windows_version_vista";
		break;
	case windows_version_7:
		return "windows_version_7";
		break;
	case windows_version_8:
		return "windows_version_8";
		break;
	case windows_version_8_1:
		return "windows_version_8_1";
		break;
	case windows_version_10:
		return "windows_version_10";
		break;
	default:
		return "windows_version_none";
		break;
	}
}
method_t UserInfoManager::string_to_method(std::string in){
	if (in == "method_none")
		return method_t::method_none;
	else if (in == "method_post")
		return method_t::method_post;
	else if (in == "method_get")
		return method_t::method_get;
	else if (in == "method_request")
		return method_t::method_request;
	else if (in == "method_update")
		return method_t::method_update;
	else if (in == "method_delete")
		return method_t::method_delete;

	return method_t::method_none;
}
language_t UserInfoManager::string_to_language(std::string in){
	if (in == "language_portuguese")
		return language_t::language_portuguese;
	else if (in == "language_english")
		return language_t::language_english;

	return language_t::language_portuguese;
}
windows_version_t UserInfoManager::string_to_windows_version(std::string in){
	if (in == "windows_version_xp")
		return windows_version_t::windows_version_xp;
	else if (in == "windows_version_vista")
		return windows_version_t::windows_version_vista;
	else if (in == "windows_version_7")
		return windows_version_t::windows_version_7;
	else if (in == "windows_version_8")
		return windows_version_t::windows_version_8;
	else if (in == "windows_version_8_1")
		return windows_version_t::windows_version_8_1;
	else if (in == "windows_version_10")
		return windows_version_t::windows_version_10;
	if (in == "windows_version_none")
		return windows_version_t::windows_version_none;

	return windows_version_t::windows_version_none;
}

std::string UserInfoManager::get_proxy_dir(){
	std::string current_proxy_dir = ProcessManager::get()->get_directory_appmain();

	boost::filesystem::path path_ = current_proxy_dir;
	return path_.branch_path().string();
}

std::string UserInfoManager::get_proxy_used(){
	std::string path_string = this->get_proxy_dir();
	boost::filesystem::path _path = path_string;

	return _path.filename().string();
}

void UserInfoManager::update_infos(){
	this->system_language = get_system_language();
	this->windows_version = get_windows_version();
	this->user_id = generate_user_id();
}
language_t UserInfoManager::get_system_language() {
	uint32_t language_id = (uint32_t)GetUserDefaultUILanguage();

	if (language_id == 1046 || language_id == 2070)
		return language_t::language_portuguese;

	return language_t::language_english;
}
windows_version_t UserInfoManager::get_windows_version() {

	if (IsWindows8Point1OrGreater())
		return windows_version_t::windows_version_10;
	else if (IsWindows8OrGreater())
		return windows_version_t::windows_version_8;
	else if (IsWindows7SP1OrGreater())
		return windows_version_t::windows_version_7;
	else if (IsWindows7OrGreater())
		return windows_version_t::windows_version_7;
	else if (IsWindowsVistaSP2OrGreater())
		return windows_version_t::windows_version_vista;
	else if (IsWindowsVistaSP1OrGreater())
		return windows_version_t::windows_version_vista;
	else if (IsWindowsVistaOrGreater())
		return windows_version_t::windows_version_vista;
	else if (IsWindowsXPSP3OrGreater())
		return windows_version_t::windows_version_xp;
	else if (IsWindowsXPSP2OrGreater())
		return windows_version_t::windows_version_xp;
	else if (IsWindowsXPSP1OrGreater())
		return windows_version_t::windows_version_xp;
	else if (IsWindowsXPOrGreater())
		return windows_version_t::windows_version_xp;

	return windows_version_t::windows_version_none;
}
uint32_t UserInfoManager::get_user_id(){
	return this->user_id;
}
uint32_t UserInfoManager::generate_user_id(){
	std::string temp = Tools::GetCharVal(HKEY_CURRENT_USER, "SOFTWARE\\ProxySync\\", "machine_id");
	uint32_t retval_id = 0;

	if (temp.empty() || temp == ""){
		srand(time(0));
		retval_id = (rand() % 999999);
	}

	try{
		retval_id = boost::lexical_cast<uint32_t>(temp);
	}
	catch (...){
		srand(time(0));
		retval_id = (rand() % 999999);
	}

	return retval_id;
}

std::string UserInfoManager::get_user_email_regedit(){
	std::string email_temp = Tools::GetCharVal(HKEY_CURRENT_USER, "SOFTWARE\\ProxySync\\", "user_email");
	if (email_temp.empty() || email_temp == "")
		return "";

	return email_temp;
}

void UserInfoManager::set_first_time(bool in){
	this->first_time = in;
}
void UserInfoManager::parse_json_to_class(Json::Value json_value){
	Json::Value json_user_id = json_value["user_id"];
	if (!json_user_id.isNull() && !json_user_id.empty())
		this->user_id = json_user_id.asInt();

	Json::Value json_firt_time = json_value["first_time"];
	if (!json_firt_time.isNull() && !json_firt_time.empty())
		this->first_time = json_firt_time.asBool();

	Json::Value json_system_language = json_value["system_language"];
	if (!json_system_language.isNull() && !json_system_language.empty()){
		std::string language_string = json_system_language.asString();
		this->system_language = this->string_to_language(language_string);
	}

	Json::Value json_windows_version = json_value["windows_version"];
	if (!json_windows_version.isNull() && !json_windows_version.empty()){
		std::string windows_version_string = json_windows_version.asString();
		this->windows_version = this->string_to_windows_version(windows_version_string);
	}
	Json::Value json_current_execute_id = json_value["current_execute_id"];
	if (!json_current_execute_id.isNull() || !json_current_execute_id.empty())
		this->current_execute_id = json_current_execute_id.asInt();
}
Json::Value UserInfoManager::parse_class_to_json(){
	Json::Value json_value;

	std::string system_language = language_t_string(this->system_language);
	std::string windows_version = windows_version_string(this->windows_version);

	json_value["user_id"] = this->user_id;
	json_value["first_time"] = this->first_time;
	json_value["system_language"] = this->system_language;
	json_value["windows_version"] = this->windows_version;
	json_value["current_execute_id"] = this->current_execute_id;

	return json_value;
}

void UserInfoManager::parse_json_to_process_info(Json::Value json_value){
	if (json_value.isNull() || json_value.empty())
		return;

	this->list_process_info_from_sql.clear();

	for (auto member : json_value){
		std::shared_ptr<ProcessInfoFromSQL> new_info = std::make_shared<ProcessInfoFromSQL>();

		Json::Value json_process_id = member["process_id"];
		std::string temp = json_process_id.toStyledString();
		if (!json_process_id.isNull() && !json_process_id.empty())
			new_info->process_id = Tools::string_to_uint32(json_process_id.asString());

		Json::Value json_process_name = member["process_name"];
		if (!json_process_name.isNull() && !json_process_name.empty())
			new_info->process_name = json_process_name.asString();

		Json::Value json_process_register = member["process_register"];
		if (!json_process_register.empty() && !json_process_register.isNull()){
			uint32_t register_uint = Tools::string_to_uint32(json_process_register.asString());
			new_info->process_register = (bool)register_uint;
		}

		this->add_list_process_info_from_sql(new_info);
	}
}
void UserInfoManager::parse_json_to_user_process_info(Json::Value json_value){
	if (json_value.isNull() || json_value.empty())
		return;

	for (auto member : json_value){
		if (member["process_info"].isNull() || member["process_info"].empty())
			continue;

		std::string json_string = member["process_info"].asString();
		Json::Value json_value = Tools::string_to_json(json_string);

		for (auto member_temp : json_value){
			std::shared_ptr<ProcessInfoToSQL> new_info = std::make_shared<ProcessInfoToSQL>();

			Json::Value json_current_date = member_temp["current_date"];
			if (!json_current_date.isNull() && !json_current_date.empty())
				new_info->current_date = json_current_date.asString();

			Json::Value json_process_name = member_temp["process_name"];
			if (!json_process_name.isNull() && !json_process_name.empty()){
				std::string process_name_stirng = json_process_name.asString();
				new_info->process_name = Tools::replace(process_name_stirng, "-", ".");
			}

			new_info->process_is_open = false;
			this->add_list_process_info_to_sql(new_info);
		}
	}
}