#pragma once
#include "SoftwareManager.h"
#include "WebManager.h"
#include "ProxySyncManager.h"
#include "FileManager.h"
#include "Time.h"
#include "ProcessManager.h"
#include <mutex>
#include <thread>

extern void call_InitializeMainForm();
extern void call_ShowUpdateForm(std::string, std::string, std::vector<std::string>);
extern void call_ShowBalloon(std::string, std::string, int);

void SoftwareManager::show_form_update(std::string title, std::string message, std::vector<std::string> vector_images){
	std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
	call_ShowUpdateForm(title, message, vector_images);
}
std::string SoftwareManager::execute_action_t(action_t action,Json::Value json_args){
	switch (action){
	case action_t_refresh:{
		return "true";
		break;
	}
	case action_t_appmain_is_open:{
		return ProcessManager::get()->appmain_is_open() ? std::string("true") : std::string("false");
		break;
	}
	case action_t_close_and_update:{
		int count_try = 0;
		while (count_try < 3){
			ProcessManager::get()->open_process(LAUNCHER_PATH);
			Sleep(2 * 1000);
			if (ProcessManager::get()->launcher_is_open())
				break;

			count_try++;
			Sleep(5 * 1000);
		}

		ProcessManager::get()->kill_current_process();
		break;
	}
	case action_t_send_message:{
		std::shared_ptr<FormInfo> form_info = std::shared_ptr<FormInfo>(new FormInfo);

		form_info->json_args = json_args; 
		form_info->action = action;

		((std::mutex*)mtx_uint)->lock();
		if (is_runnings){
			((std::mutex*)mtx_uint)->unlock();
			return "false";
		}
		
		this->current_show_form = form_info;
		this->active_thread_form = true;
		((std::mutex*)mtx_uint)->unlock();
		
		TimeChronometer timer;
		timer.reset();
		while ((uint32_t)timer.elapsed_milliseconds() < 10 * 1000){
			if (!active_thread_form)
				break;

			Sleep(2 * 1000);
		}

		((std::mutex*)mtx_uint)->lock();
		std::string result = current_show_form->result;
		((std::mutex*)mtx_uint)->unlock();

		return result;
		break;
	}
	case action_t_show_tooltip:{
		return "false";
		break;
	}
	default:{
		return "false";
		break;
	}
	}
	return "false";
}

SoftwareManager::SoftwareManager(){
	mtx_uint = (uint32_t*)new std::mutex();

	std::cout << "\n new version 1.0.3";
	FileManager::get()->create_dir("C:\\ProgramData\\ProxySync");

	Json::Value json_value = FileManager::get()->load_json_file("C:\\ProgramData\\ProxySync\\info_proxy.json");
	if (json_value.isNull() || json_value.empty()){
		std::cout << "\n Json is NULL";

		UserInfoManager::get()->update_infos();
		UserInfoManager::get()->set_first_time(false);

		Json::Value json_to_save = UserInfoManager::get()->parse_class_to_json();
		FileManager::get()->save_josn_file("C:\\ProgramData\\ProxySync\\info_proxy.json", json_to_save);
	}
	else{
		std::cout << "\n Json is not NULL";
		
		std::cout << "\n content json : " << json_value.toStyledString();
		UserInfoManager::get()->parse_json_to_class(json_value);
	}

	ProxySyncManager::get();
	thread_execute_actions();
	init_thread();
}
SoftwareManager::~SoftwareManager(){
	delete ((std::mutex*)mtx_uint);
}

void SoftwareManager::thread_execute_actions(){
	std::thread([&](){
		TimeChronometer timer;
		while (true){
			Sleep(5 * 1000);

			if (!active_thread_form || !current_show_form)
				continue; 

			((std::mutex*)mtx_uint)->lock();
			current_show_form->is_running = true;
			current_show_form->result = "true";
			is_runnings = true;
			Json::Value json_args = current_show_form->json_args;
			((std::mutex*)mtx_uint)->unlock();

			this->wait_idle_timeout(3 * 60 *  1000);

			std::string message = "";
			std::string title = "";
		
			if (!json_args["message"].isNull() && !json_args["message"].empty())
				message = json_args["message"].asString();

			if (!json_args["title"].isNull() && !json_args["title"].empty())
				title = json_args["title"].asString();

			std::vector<std::string> vector_images;
			if (!json_args["array_images"].isNull() && !json_args["array_images"].empty()){
				for (uint32_t index = 0; index < json_args["array_images"].size(); index++)
					vector_images.push_back(json_args["array_images"][index].asString());
			}

			show_form_update(title, message, vector_images);

			((std::mutex*)mtx_uint)->lock();
			this->current_show_form = nullptr;
			this->active_thread_form = false;
			is_runnings = false;
			((std::mutex*)mtx_uint)->unlock();
		}
	}).detach();
}
void SoftwareManager::start_loop(){
	while (true){
		Sleep(10 * 1000);

		std::cout << "\n\nTime IDLE: " << UserInfoManager::get()->get_time_idle();
		
		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		if (!WebManager::get()->check_connection())
			continue;

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		if (!WebManager::get()->refresh_request_action())
			continue;

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		uint32_t current_execute_id = UserInfoManager::get()->get_current_execute_id();
		std::shared_ptr<ActionInfoResult> action_result = this->generate_action_info_result(current_execute_id);
		if (!action_result)
			continue;

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		UserInfoManager::get()->add_execute_action_result_list(action_result);

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		Json::Value& action_execute_list_json = UserInfoManager::get()->execute_action_result_list_to_json();
		if (action_execute_list_json.empty() || action_execute_list_json.isNull())
			continue;

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		std::string _temp = action_execute_list_json.toStyledString();
		_temp = Tools::remove_new_line(_temp);
		_temp = Tools::remove_space(_temp);

		int try_count = 0;
		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		while (try_count <= 3){
			std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
			std::string response = WebManager::get()->send_infos(Tools::string_trim(_temp));

			std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
			if (Tools::check_have_string({ "update", "success", "create" }, response))
				break;

			std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
			try_count++;
		}

		std::cout << "\nfunction: " << __FUNCTION__ << " - line: " << __LINE__;
		UserInfoManager::get()->clean_execute_action_list();
	}
}
void SoftwareManager::init_thread(){
	while (!WebManager::get()->check_connection())
		Sleep(500);
	
	uint32_t current_execute_id = WebManager::get()->get_last_execute_id();	
	UserInfoManager::get()->set_current_execute_id((current_execute_id + 1));
	uint32_t current_user_id = UserInfoManager::get()->get_user_id();

	std::string proxy_used = WebManager::get()->get_proxy_used(current_user_id);
	UserInfoManager::get()->set_current_proxy_used(proxy_used);
	
	std::string json_user_process_info = WebManager::get()->get_user_process_info(current_user_id);	
	UserInfoManager::get()->parse_json_to_user_process_info(Tools::string_to_json(json_user_process_info));

	std::thread([&](){ thread_process_refresh(); }).detach();

	WebManager::get()->send_infos("[]");

	Sleep(5 * 1000);

	std::string user_email = UserInfoManager::get()->get_user_email_regedit();
	WebManager::get()->send_user_email(current_user_id, user_email);

	start_loop();
}

void SoftwareManager::thread_process_refresh(){
	while (true){
		Sleep(5 * 1000);

		if (!WebManager::get()->check_connection())
			continue;

		uint32_t current_user_id = UserInfoManager::get()->get_user_id();

		if (ProcessManager::get()->appmain_is_open())
			WebManager::get()->send_last_time_proxy(current_user_id);
		
		std::string json_process_info = WebManager::get()->get_process_info();
		UserInfoManager::get()->parse_json_to_process_info(Tools::string_to_json(json_process_info));

		std::vector<std::shared_ptr<ProcessInfo>> list_process_open = ProcessManager::get()->get_process_running();
		std::vector<std::shared_ptr<ProcessInfoFromSQL>> list_process_sql = UserInfoManager::get()->get_list_process_info_from_sql();
		
		for (auto process_sql : list_process_sql){
			if (!process_sql->process_register)
				continue;

			std::shared_ptr<ProcessInfoToSQL> new_process_info_to_sql = std::make_shared<ProcessInfoToSQL>();
			
			std::string process_name_sql = process_sql->process_name;
			process_name_sql = Tools::string_trim(process_name_sql);
			process_name_sql = Tools::lower(process_name_sql);

			new_process_info_to_sql->process_name = process_name_sql;

			for (auto process_open : list_process_open){
				std::string process_name = process_open->process_name;
				process_name = Tools::string_trim(process_name);
				process_name = Tools::lower(process_name);

				if (process_name_sql != process_name)
					continue;
								
				new_process_info_to_sql->current_date = Tools::get_current_time();
				new_process_info_to_sql->process_is_open = true;
			}

			UserInfoManager::get()->add_list_process_info_to_sql(new_process_info_to_sql);
		}

		std::string json_to_send = UserInfoManager::get()->parse_process_info_to_json();
		json_to_send = Tools::remove_space(json_to_send);
		json_to_send = Tools::remove_new_line(json_to_send);
		json_to_send = Tools::replace(json_to_send, "[.]", "-");

		WebManager::get()->send_process_infos(current_user_id, json_to_send);
	}
}

std::shared_ptr<ActionInfoResult> SoftwareManager::generate_action_info_result(uint32_t current_execute_action_id){
	std::shared_ptr<ActionInfoResult> retval;

	std::shared_ptr<ActionInfo> action_to_execute = UserInfoManager::get()->get_execute_action_info(current_execute_action_id);
	if (!action_to_execute)
		return retval;

	if (action_to_execute->minutes_offline <= 5){
		action_t actiont_t_execute = (action_t)action_to_execute->action_id;
		Json::Value json_args = action_to_execute->action_args;
		language_t language = action_to_execute->language;
		language_t language_system = UserInfoManager::get()->get_system_language();

		std::string proxy_used = action_to_execute->proxy_send;
		proxy_used = Tools::lower(proxy_used);
		proxy_used = Tools::replace(proxy_used," ","");
		proxy_used = Tools::string_trim(proxy_used) != "" ? proxy_used : "null";
		
		std::string current_proxy_used = UserInfoManager::get()->get_current_proxy_used();
		current_proxy_used = Tools::lower(current_proxy_used);
		current_proxy_used = Tools::replace(current_proxy_used, "_", "");
		current_proxy_used = Tools::string_trim(current_proxy_used) != "" ? current_proxy_used : "null";

		std::cout << "\nProxy_used: " << proxy_used;
		std::cout << "\nCurrent_proxy_used: " << current_proxy_used;

		std::string result = "false";
		if (actiont_t_execute == action_t_send_message && (language == language_system || language == language_all)){
			if (proxy_used == current_proxy_used || proxy_used == "all"){
				uint32_t days_without_open = UINT32_MAX;
				if (!json_args["days_inactive"].isNull() && !json_args["days_inactive"].empty())
					days_without_open = Tools::string_to_uint32(json_args["days_inactive"].asString());
				
				uint32_t current_user_id = UserInfoManager::get()->get_user_id();
				std::string last_time_open_string = WebManager::get()->get_last_time_proxy(current_user_id);
				
				int days_without_proxy = Tools::string_to_uint32(last_time_open_string);
				days_without_proxy = days_without_proxy / (60 * 24);

				if (days_without_proxy >= days_without_open){
					UserInfoManager::get()->set_current_execute_id_form(current_execute_action_id);
					result = this->execute_action_t(actiont_t_execute, json_args);
				}
			}
		}
		else if (actiont_t_execute != action_t_send_message){
			result = this->execute_action_t(actiont_t_execute, json_args);
		}
		else{
			result = "false";  
		}
		
		retval = std::make_shared<ActionInfoResult>();
		retval->action_id = action_to_execute->action_id;
		retval->execute_id = current_execute_action_id;
		retval->action_name = action_to_execute->action_name;
		retval->action_result = result;
	}

	UserInfoManager::get()->update_current_execute_id();
	return retval;
}