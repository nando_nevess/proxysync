#pragma once
#include "Utility.h"
#include <direct.h>

class FileManager {

public:
	Json::Value load_json_file(std::string filename) {
		std::ifstream jsonGet;
		Json::Value root;
		Json::Reader reader;

		jsonGet.open(filename);
		reader.parse(jsonGet, root);
		jsonGet.close();

		return root;
	}
	bool save_josn_file(std::string file_name, Json::Value json_value){
		std::ofstream jsonSet;

		jsonSet.open(file_name);
		if (!jsonSet.is_open())
			return false;

		jsonSet << json_value.toStyledString();
		jsonSet.close();
		return true;
	}

	bool create_dir(const std::string& path){
		if (check_dir_exist(path))
			return false;
		else
			mkdir(&path[0]);
		
		return true;
	}
	bool check_dir_exist(const std::string& dirName_in){
		DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!

		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!

		return false;    // this is not a directory!
	}

	static FileManager* get() {
		static FileManager* m = nullptr;
		if (!m)
			m = new FileManager();

		return m;
	}
};
