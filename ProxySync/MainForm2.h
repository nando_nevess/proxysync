#pragma once
#include "ProxySyncManager.h"
#include <iostream>
#include <Windows.h>

#pragma comment(lib, "gdi32.lib")

namespace ProxySync {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;
	using namespace System::Net;
	using namespace System::IO;

	public ref class MainForm2 : public System::Windows::Forms::Form {
	
	public:MainForm2() {
		InitializeComponent();
	}
	protected:~MainForm2() {
		if (components)
			delete components;
	}

	protected:

	private: System::Windows::Forms::TextBox^  textBox1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::WebBrowser^  webBrowser1;
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			void InitializeComponent(void) {
				this->textBox1 = (gcnew System::Windows::Forms::TextBox());
				this->button1 = (gcnew System::Windows::Forms::Button());
				this->webBrowser1 = (gcnew System::Windows::Forms::WebBrowser());
				this->SuspendLayout();
				// 
				// textBox1
				// 
				this->textBox1->Location = System::Drawing::Point(0, 11);
				this->textBox1->Name = L"textBox1";
				this->textBox1->Size = System::Drawing::Size(458, 20);
				this->textBox1->TabIndex = 1;
				// 
				// button1
				// 
				this->button1->Location = System::Drawing::Point(472, 9);
				this->button1->Name = L"button1";
				this->button1->Size = System::Drawing::Size(75, 23);
				this->button1->TabIndex = 2;
				this->button1->Text = L"button1";
				this->button1->UseVisualStyleBackColor = true;
				this->button1->Click += gcnew System::EventHandler(this, &MainForm2::button1_Click);
				// 
				// webBrowser1
				// 
				this->webBrowser1->Location = System::Drawing::Point(97, 141);
				this->webBrowser1->MinimumSize = System::Drawing::Size(20, 20);
				this->webBrowser1->Name = L"webBrowser1";
				this->webBrowser1->Size = System::Drawing::Size(250, 250);
				this->webBrowser1->TabIndex = 3;
				// 
				// MainForm2
				// 
				this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				this->ClientSize = System::Drawing::Size(559, 507);
				this->Controls->Add(this->webBrowser1);
				this->Controls->Add(this->button1);
				this->Controls->Add(this->textBox1);
				this->Name = L"MainForm2";
				this->Text = L"MainForm2";
				this->ResumeLayout(false);
				this->PerformLayout();

			}
#pragma endregion

			String^ get_url_id() {
				String^ url = textBox1->Text;
				if (url->Trim()->ToLower() == "")
					return String::Empty;

				System::Text::RegularExpressions::Regex^ match = gcnew System::Text::RegularExpressions::Regex("(?:https?:\/\/)?(?:youtu\.be\/|(?:www\.)?youtube\.com\/watch(?:\.php)?\?.*v=)([a-zA-Z0-9\-_]+)");
				System::Text::RegularExpressions::Match^ teste = match->Match(url);

				// ->Match(url);
				return teste->Success ? teste->Groups[1]->Value : String::Empty;
			}
			
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		String^ url_id = get_url_id();

		webBrowser1->Navigate("https://www.youtube.com/v/" + url_id + "?version=3?rel=0&arp;autoplay=1");
	}
	};
}
