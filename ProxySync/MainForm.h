#pragma once
#include "ProxySyncManager.h"
#include <iostream>
#include <Windows.h>

#pragma comment(lib, "gdi32.lib")

namespace ProxySync {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Runtime::InteropServices;
	using namespace System::Net;
	using namespace System::IO;

	public ref class MainForm : public System::Windows::Forms::Form{
	
	public:MainForm(String^ title, String^ message, cli::array<String^>^ array_images_){
			   InitializeComponent();

			   this->array_images_url = array_images_;
			   this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			   this->Region = System::Drawing::Region::FromHrgn((IntPtr)CreateRoundRectRgn(0, 0, this->Width, this->Height, 20, 20));

			   this->label_update_title->Text = title;
			   this->label_update_message->Text = message;

			   this->button_update_MouseLeave((System::Object^)this->button_shop_now, nullptr);
			   this->button_update_MouseLeave((System::Object^)this->button_close, nullptr);

			   this->side_right_black = this->picture_black_right->Image;
			   this->side_left_black = this->picture_black_left->Image;

			   this->array_images = gcnew cli::array<System::Tuple<Image^, String^>^>(array_images_->Length);

			   int index = 0;
			   for each(String^ ulr in array_images_){
				   array<String^>^ StringArray = ulr->Split(';');

				   String^ url_image = StringArray[0]->Trim();
				   String^ link_image = StringArray[1]->Trim();

				   Stream^ imageData = download_data(url_image);
				   if (!imageData)
					   continue;

				   try{
					   Image^ img = Image::FromStream(imageData);
					   this->array_images[index] = gcnew System::Tuple<Image^, String^>(img, link_image);
					   imageData->Close();
				   }
				   catch (...){
					   continue;
				   }

				   index++;
			   }

			   count_images = index;
			   this->picture_side->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;

			   this->picture_black_right_Click(nullptr, nullptr);
	}
	protected:~MainForm(){
				  if (components)
					  delete components;
	}

	protected:
	private: System::Windows::Forms::Label^  label_update_message;
	private: Telerik::WinControls::UI::RadPanel^  panel_logo;
	private: Telerik::WinControls::UI::RadPanel^  panel_header;
	private: Telerik::WinControls::UI::RadButton^  button_close;
	private: Telerik::WinControls::UI::RadButton^  button_shop_now;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: Telerik::WinControls::UI::RadLabel^  label_update_title;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::PictureBox^  picture_side;
	private: Telerik::WinControls::UI::RadPanel^  panel_picture;
	private: System::Windows::Forms::PictureBox^  picture_black_left;
	private: System::Windows::Forms::PictureBox^  picture_black_right;
	private: System::Windows::Forms::Timer^  timer_side_image;
	private: System::Windows::Forms::PictureBox^  picture_left;
	private: System::Windows::Forms::PictureBox^  picture_right;
	private: System::ComponentModel::IContainer^  components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void){
				 this->components = (gcnew System::ComponentModel::Container());
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MainForm::typeid));
				 this->label_update_message = (gcnew System::Windows::Forms::Label());
				 this->panel1 = (gcnew System::Windows::Forms::Panel());
				 this->button_close = (gcnew Telerik::WinControls::UI::RadButton());
				 this->button_shop_now = (gcnew Telerik::WinControls::UI::RadButton());
				 this->panel_logo = (gcnew Telerik::WinControls::UI::RadPanel());
				 this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
				 this->panel_header = (gcnew Telerik::WinControls::UI::RadPanel());
				 this->label_update_title = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
				 this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
				 this->picture_side = (gcnew System::Windows::Forms::PictureBox());
				 this->panel_picture = (gcnew Telerik::WinControls::UI::RadPanel());
				 this->picture_black_left = (gcnew System::Windows::Forms::PictureBox());
				 this->picture_black_right = (gcnew System::Windows::Forms::PictureBox());
				 this->timer_side_image = (gcnew System::Windows::Forms::Timer(this->components));
				 this->picture_left = (gcnew System::Windows::Forms::PictureBox());
				 this->picture_right = (gcnew System::Windows::Forms::PictureBox());
				 this->panel1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->button_close))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->button_shop_now))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_logo))->BeginInit();
				 this->panel_logo->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_header))->BeginInit();
				 this->panel_header->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_update_title))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_side))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_picture))->BeginInit();
				 this->panel_picture->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_black_left))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_black_right))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_left))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_right))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // label_update_message
				 // 
				 this->label_update_message->AutoSize = true;
				 this->label_update_message->Font = (gcnew System::Drawing::Font(L"Century Gothic", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label_update_message->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(41)),
					 static_cast<System::Int32>(static_cast<System::Byte>(53)), static_cast<System::Int32>(static_cast<System::Byte>(65)));
				 this->label_update_message->Location = System::Drawing::Point(428, 76);
				 this->label_update_message->Name = L"label_update_message";
				 this->label_update_message->Size = System::Drawing::Size(159, 17);
				 this->label_update_message->TabIndex = 2;
				 this->label_update_message->Text = L"label_update_message";
				 this->label_update_message->Visible = false;
				 // 
				 // panel1
				 // 
				 this->panel1->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(41)), static_cast<System::Int32>(static_cast<System::Byte>(53)),
					 static_cast<System::Int32>(static_cast<System::Byte>(65)));
				 this->panel1->Controls->Add(this->button_close);
				 this->panel1->Controls->Add(this->button_shop_now);
				 this->panel1->Dock = System::Windows::Forms::DockStyle::Bottom;
				 this->panel1->Location = System::Drawing::Point(0, 470);
				 this->panel1->Name = L"panel1";
				 this->panel1->Size = System::Drawing::Size(630, 39);
				 this->panel1->TabIndex = 9;
				 // 
				 // button_close
				 // 
				 this->button_close->DisplayStyle = Telerik::WinControls::DisplayStyle::Text;
				 this->button_close->Location = System::Drawing::Point(463, -1);
				 this->button_close->Name = L"button_close";
				 this->button_close->Size = System::Drawing::Size(167, 40);
				 this->button_close->TabIndex = 5;
				 this->button_close->Text = L"Close";
				 this->button_close->Click += gcnew System::EventHandler(this, &MainForm::button_close_Click);
				 this->button_close->MouseEnter += gcnew System::EventHandler(this, &MainForm::button_update_MouseEnter);
				 this->button_close->MouseLeave += gcnew System::EventHandler(this, &MainForm::button_update_MouseLeave);
				 (cli::safe_cast<Telerik::WinControls::UI::RadButtonElement^>(this->button_close->GetChildAt(0)))->DisplayStyle = Telerik::WinControls::DisplayStyle::Text;
				 (cli::safe_cast<Telerik::WinControls::UI::RadButtonElement^>(this->button_close->GetChildAt(0)))->Text = L"Close";
				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->ForeColor = System::Drawing::Color::LightGray;
				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->Alignment = System::Drawing::ContentAlignment::MiddleCenter;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->ForeColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->ForeColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->ForeColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->InnerColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->InnerColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->InnerColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->InnerColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->ForeColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_close->GetChildAt(0)->GetChildAt(2)))->BackColor = System::Drawing::Color::Transparent;
				 // 
				 // button_shop_now
				 // 
				 this->button_shop_now->DisplayStyle = Telerik::WinControls::DisplayStyle::Text;
				 this->button_shop_now->Location = System::Drawing::Point(296, -1);
				 this->button_shop_now->Name = L"button_shop_now";
				 this->button_shop_now->Size = System::Drawing::Size(167, 40);
				 this->button_shop_now->TabIndex = 6;
				 this->button_shop_now->Text = L"Shop Now";
				 this->button_shop_now->Click += gcnew System::EventHandler(this, &MainForm::button_shop_now_Click);
				 this->button_shop_now->MouseEnter += gcnew System::EventHandler(this, &MainForm::button_shop_now_MouseEnter);
				 this->button_shop_now->MouseLeave += gcnew System::EventHandler(this, &MainForm::button_shop_now_MouseLeave);
				 (cli::safe_cast<Telerik::WinControls::UI::RadButtonElement^>(this->button_shop_now->GetChildAt(0)))->DisplayStyle = Telerik::WinControls::DisplayStyle::Text;
				 (cli::safe_cast<Telerik::WinControls::UI::RadButtonElement^>(this->button_shop_now->GetChildAt(0)))->Text = L"Shop Now";
				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->ForeColor = System::Drawing::Color::LightGray;
				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->Alignment = System::Drawing::ContentAlignment::MiddleCenter;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->ForeColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->ForeColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->ForeColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->InnerColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->InnerColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->InnerColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->InnerColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->ForeColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->button_shop_now->GetChildAt(0)->GetChildAt(2)))->BackColor = System::Drawing::Color::Transparent;
				 // 
				 // panel_logo
				 // 
				 this->panel_logo->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(21)), static_cast<System::Int32>(static_cast<System::Byte>(33)),
					 static_cast<System::Int32>(static_cast<System::Byte>(45)));
				 this->panel_logo->Controls->Add(this->pictureBox1);
				 this->panel_logo->Location = System::Drawing::Point(0, 0);
				 this->panel_logo->Name = L"panel_logo";
				 this->panel_logo->Size = System::Drawing::Size(65, 50);
				 this->panel_logo->TabIndex = 8;
				 this->panel_logo->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::MainForm_MouseDown);
				 this->panel_logo->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::MainForm_MouseMove);
				 this->panel_logo->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::MainForm_MouseUp);
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->ForeColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->ForeColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->ForeColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->InnerColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->InnerColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->InnerColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->InnerColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_logo->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Transparent;
				 // 
				 // pictureBox1
				 // 
				 this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
				 this->pictureBox1->Location = System::Drawing::Point(17, 5);
				 this->pictureBox1->Name = L"pictureBox1";
				 this->pictureBox1->Size = System::Drawing::Size(35, 35);
				 this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
				 this->pictureBox1->TabIndex = 0;
				 this->pictureBox1->TabStop = false;
				 // 
				 // panel_header
				 // 
				 this->panel_header->BackColor = System::Drawing::Color::White;
				 this->panel_header->Controls->Add(this->label_update_title);
				 this->panel_header->Controls->Add(this->pictureBox3);
				 this->panel_header->Controls->Add(this->pictureBox2);
				 this->panel_header->Location = System::Drawing::Point(65, 0);
				 this->panel_header->Name = L"panel_header";
				 this->panel_header->Size = System::Drawing::Size(570, 50);
				 this->panel_header->TabIndex = 7;
				 this->panel_header->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::MainForm_MouseDown);
				 this->panel_header->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::MainForm_MouseMove);
				 this->panel_header->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MainForm::MainForm_MouseUp);
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->ForeColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->ForeColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->ForeColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->InnerColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->InnerColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->InnerColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->InnerColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_header->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Transparent;
				 // 
				 // label_update_title
				 // 
				 this->label_update_title->Font = (gcnew System::Drawing::Font(L"Century Gothic", 15.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
					 static_cast<System::Byte>(0)));
				 this->label_update_title->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(41)),
					 static_cast<System::Int32>(static_cast<System::Byte>(53)), static_cast<System::Int32>(static_cast<System::Byte>(65)));
				 this->label_update_title->Location = System::Drawing::Point(6, 12);
				 this->label_update_title->Name = L"label_update_title";
				 this->label_update_title->Size = System::Drawing::Size(200, 30);
				 this->label_update_title->TabIndex = 2;
				 this->label_update_title->Text = L"label_update_title";
				 // 
				 // pictureBox3
				 // 
				 this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.Image")));
				 this->pictureBox3->Location = System::Drawing::Point(495, 3);
				 this->pictureBox3->Name = L"pictureBox3";
				 this->pictureBox3->Size = System::Drawing::Size(30, 30);
				 this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
				 this->pictureBox3->TabIndex = 1;
				 this->pictureBox3->TabStop = false;
				 this->pictureBox3->Click += gcnew System::EventHandler(this, &MainForm::pictureBox3_Click);
				 // 
				 // pictureBox2
				 // 
				 this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
				 this->pictureBox2->Location = System::Drawing::Point(523, 3);
				 this->pictureBox2->Name = L"pictureBox2";
				 this->pictureBox2->Size = System::Drawing::Size(30, 30);
				 this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
				 this->pictureBox2->TabIndex = 0;
				 this->pictureBox2->TabStop = false;
				 this->pictureBox2->Click += gcnew System::EventHandler(this, &MainForm::pictureBox2_Click);
				 // 
				 // picture_side
				 // 
				 this->picture_side->Cursor = System::Windows::Forms::Cursors::Default;
				 this->picture_side->Location = System::Drawing::Point(0, 49);
				 this->picture_side->Name = L"picture_side";
				 this->picture_side->Size = System::Drawing::Size(630, 388);
				 this->picture_side->TabIndex = 10;
				 this->picture_side->TabStop = false;
				 this->picture_side->Click += gcnew System::EventHandler(this, &MainForm::picture_side_Click);
				 this->picture_side->MouseEnter += gcnew System::EventHandler(this, &MainForm::picture_side_MouseEnter);
				 this->picture_side->MouseLeave += gcnew System::EventHandler(this, &MainForm::picture_side_MouseLeave);
				 // 
				 // panel_picture
				 // 
				 this->panel_picture->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(21)), static_cast<System::Int32>(static_cast<System::Byte>(33)),
					 static_cast<System::Int32>(static_cast<System::Byte>(45)));
				 this->panel_picture->Controls->Add(this->picture_black_left);
				 this->panel_picture->Controls->Add(this->picture_black_right);
				 this->panel_picture->Location = System::Drawing::Point(0, 437);
				 this->panel_picture->Name = L"panel_picture";
				 this->panel_picture->Size = System::Drawing::Size(630, 33);
				 this->panel_picture->TabIndex = 11;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->ForeColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->ForeColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->ForeColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->InnerColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->InnerColor2 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->InnerColor3 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->InnerColor4 = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->ForeColor = System::Drawing::Color::Transparent;
				 (cli::safe_cast<Telerik::WinControls::Primitives::BorderPrimitive^>(this->panel_picture->GetChildAt(0)->GetChildAt(1)))->BackColor = System::Drawing::Color::Transparent;
				 // 
				 // picture_black_left
				 // 
				 this->picture_black_left->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"picture_black_left.Image")));
				 this->picture_black_left->Location = System::Drawing::Point(0, 1);
				 this->picture_black_left->Name = L"picture_black_left";
				 this->picture_black_left->Size = System::Drawing::Size(40, 32);
				 this->picture_black_left->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
				 this->picture_black_left->TabIndex = 2;
				 this->picture_black_left->TabStop = false;
				 this->picture_black_left->Click += gcnew System::EventHandler(this, &MainForm::picture_black_left_Click);
				 this->picture_black_left->MouseEnter += gcnew System::EventHandler(this, &MainForm::picture_black_left_MouseEnter);
				 this->picture_black_left->MouseLeave += gcnew System::EventHandler(this, &MainForm::picture_black_left_MouseLeave);
				 // 
				 // picture_black_right
				 // 
				 this->picture_black_right->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"picture_black_right.Image")));
				 this->picture_black_right->Location = System::Drawing::Point(589, 1);
				 this->picture_black_right->Name = L"picture_black_right";
				 this->picture_black_right->Size = System::Drawing::Size(40, 32);
				 this->picture_black_right->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
				 this->picture_black_right->TabIndex = 0;
				 this->picture_black_right->TabStop = false;
				 this->picture_black_right->Click += gcnew System::EventHandler(this, &MainForm::picture_black_right_Click);
				 this->picture_black_right->MouseEnter += gcnew System::EventHandler(this, &MainForm::picture_black_right_MouseEnter);
				 this->picture_black_right->MouseLeave += gcnew System::EventHandler(this, &MainForm::picture_black_right_MouseLeave);
				 // 
				 // timer_side_image
				 // 
				 this->timer_side_image->Enabled = true;
				 this->timer_side_image->Interval = 30000;
				 this->timer_side_image->Tick += gcnew System::EventHandler(this, &MainForm::timer_side_image_Tick);
				 // 
				 // picture_left
				 // 
				 this->picture_left->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"picture_left.Image")));
				 this->picture_left->Location = System::Drawing::Point(314, 95);
				 this->picture_left->Name = L"picture_left";
				 this->picture_left->Size = System::Drawing::Size(40, 32);
				 this->picture_left->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
				 this->picture_left->TabIndex = 3;
				 this->picture_left->TabStop = false;
				 this->picture_left->Visible = false;
				 // 
				 // picture_right
				 // 
				 this->picture_right->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"picture_right.Image")));
				 this->picture_right->Location = System::Drawing::Point(360, 95);
				 this->picture_right->Name = L"picture_right";
				 this->picture_right->Size = System::Drawing::Size(40, 32);
				 this->picture_right->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
				 this->picture_right->TabIndex = 12;
				 this->picture_right->TabStop = false;
				 this->picture_right->Visible = false;
				 // 
				 // MainForm
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->BackColor = System::Drawing::Color::Gainsboro;
				 this->ClientSize = System::Drawing::Size(630, 509);
				 this->Controls->Add(this->picture_right);
				 this->Controls->Add(this->picture_left);
				 this->Controls->Add(this->panel_picture);
				 this->Controls->Add(this->label_update_message);
				 this->Controls->Add(this->picture_side);
				 this->Controls->Add(this->panel_logo);
				 this->Controls->Add(this->panel1);
				 this->Controls->Add(this->panel_header);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->Name = L"MainForm";
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"ProxySync";
				 this->panel1->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->button_close))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->button_shop_now))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_logo))->EndInit();
				 this->panel_logo->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_header))->EndInit();
				 this->panel_header->ResumeLayout(false);
				 this->panel_header->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_update_title))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_side))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_picture))->EndInit();
				 this->panel_picture->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_black_left))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_black_right))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_left))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->picture_right))->EndInit();
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion

	private: bool dragging = false;
	private: System::Drawing::Point dragCursorPoint;
	private: System::Drawing::Point dragFormPoint;
	private: cli::array<String^>^ array_images_url;
	private: int current_image_position = 0;
	private: cli::array<System::Tuple<Image^, String^>^>^ array_images;
	private: Image^ side_right_black;
	private: Image^ side_left_black;
	private: int count_images = 0;
	private: bool disable_not = false;
			 private: bool send_click_count = false;

	private: System::Void button_update_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor = Color::FromArgb(41, 53, 65);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor2 = Color::FromArgb(41, 53, 65);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor3 = Color::FromArgb(41, 53, 65);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor4 = Color::FromArgb(41, 53, 65);

				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(((Telerik::WinControls::UI::RadButton^)sender)->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->ForeColor = System::Drawing::Color::LightGray;
	}
	private: System::Void button_update_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor = Color::FromArgb(21, 33, 45);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor2 = Color::FromArgb(21, 33, 45);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor3 = Color::FromArgb(21, 33, 45);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor4 = Color::FromArgb(21, 33, 45);

				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(((Telerik::WinControls::UI::RadButton^)sender)->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->ForeColor = Color::FromArgb(229, 129, 49);
	}

	private: System::Void picture_side_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
				 if (!array_images[current_image_position])
					 return;

				 String^ image_link = array_images[current_image_position]->Item2;
				 if (image_link->Trim()->ToLower() == "null")
					 return;

				 this->picture_side->Cursor = System::Windows::Forms::Cursors::Hand;
	}
	private: System::Void picture_side_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
				 this->picture_side->Cursor = System::Windows::Forms::Cursors::Default;
	}

	private: System::Void MainForm_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 dragging = true;
				 dragCursorPoint = System::Windows::Forms::Cursor::Position;
				 dragFormPoint = this->Location;
	}
	private: System::Void MainForm_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 if (!dragging)
					 return;

				 System::Drawing::Point dif = System::Drawing::Point::Subtract(System::Windows::Forms::Cursor::Position, System::Drawing::Size(dragCursorPoint));
				 this->Location = System::Drawing::Point::Add(dragFormPoint, System::Drawing::Size(dif));
	}
	private: System::Void MainForm_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				 dragging = false;
	}

	private: System::Void pictureBox3_Click(System::Object^  sender, System::EventArgs^  e) {
				 this->WindowState = FormWindowState::Minimized;
	}
	private: System::Void pictureBox2_Click(System::Object^  sender, System::EventArgs^  e) {
				 this->Close();
	}

	private: System::Void picture_black_right_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
				 this->picture_black_right->Image = this->picture_right->Image;
	}
	private: System::Void picture_black_right_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
				 this->picture_black_right->Image = this->side_right_black;
	}

	private: System::Void picture_black_left_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
				 this->picture_black_left->Image = this->picture_left->Image;
	}
	private: System::Void picture_black_left_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
				 this->picture_black_left->Image = this->side_left_black;
	}

			 Stream^ download_data(String^ url){
				 try{
					 WebRequest^ req = WebRequest::Create(url);
					 WebResponse^ response = req->GetResponse();
					 return response->GetResponseStream();
				 }
				 catch (Exception^){
					 return nullptr;
				 }

				 return nullptr;
			 }

	private: System::Void timer_side_image_Tick(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_not)
					 return;

				 disable_not = true;
				 current_image_position++;

				 int size_array_images = count_images;
				 if (current_image_position >= size_array_images)
					 current_image_position = 0;

				 if (!array_images[current_image_position]){
					 disable_not = false;
					 return;
				 }

				 this->picture_side->Image = array_images[current_image_position]->Item1;
				 disable_not = false;
	}

	private: System::Void picture_black_right_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_not)
					 return;

				 disable_not = true;
				 current_image_position++;

				 int size_array_images = count_images;
				 if (current_image_position >= size_array_images)
					 current_image_position = 0;

				 if (!array_images[current_image_position]){
					 disable_not = false;
					 return;
				 }

				 this->picture_side->Image = array_images[current_image_position]->Item1;
				 disable_not = false;
	}
	private: System::Void picture_black_left_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_not)
					 return;

				 disable_not = true;
				 current_image_position--;

				 int size_array_images = count_images;
				 if (current_image_position < 0)
					 current_image_position = size_array_images - 1;

				 if (!array_images[current_image_position]){
					 disable_not = false;
					 return;
				 }

				 this->picture_side->Image = array_images[current_image_position]->Item1;
				 disable_not = false;
	}

	private: System::Void button_close_Click(System::Object^  sender, System::EventArgs^  e) {
				 this->Close();
	}
	private: System::Void button_shop_now_Click(System::Object^  sender, System::EventArgs^  e) {
				 //System::Diagnostics::ProcessStartInfo^ sInfo = gcnew System::Diagnostics::ProcessStartInfo("https://nptunnel.com/");
				 //System::Diagnostics::Process::Start(sInfo);

				 if (!send_click_count)
					 ProxySyncManager::get()->call_send_insert_click_count();

				 send_click_count = true;
	}

	private: System::Void picture_side_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_not)
					 return;

				 disable_not = true;
				 if (!array_images[current_image_position]){
					 disable_not = false;
					 return;
				 }

				 String^ image_link = array_images[current_image_position]->Item2;
				 if (image_link->Trim()->ToLower() == "null"){
					 disable_not = false;
					 return;
				 }

				 System::Diagnostics::ProcessStartInfo^ sInfo = gcnew System::Diagnostics::ProcessStartInfo(image_link);
				 try{
					 System::Diagnostics::Process::Start(sInfo);
				 }
				 catch (...){
				 }

				 disable_not = false;

				 if (!send_click_count)
					 ProxySyncManager::get()->call_send_insert_click_count();

				 send_click_count = true;
	}

	private: System::Void button_shop_now_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor = Color::FromArgb(21, 33, 45);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor2 = Color::FromArgb(21, 33, 45);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor3 = Color::FromArgb(21, 33, 45);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor4 = Color::FromArgb(21, 33, 45);

				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(((Telerik::WinControls::UI::RadButton^)sender)->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->ForeColor = Color::FromArgb(229, 129, 49);

				 this->button_shop_now->Cursor = System::Windows::Forms::Cursors::Hand;
	}
	private: System::Void button_shop_now_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor = Color::FromArgb(41, 53, 65);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor2 = Color::FromArgb(41, 53, 65);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor3 = Color::FromArgb(41, 53, 65);
				 ((Telerik::WinControls::UI::RadButton^)sender)->ButtonElement->ButtonFillElement->BackColor4 = Color::FromArgb(41, 53, 65);

				 (cli::safe_cast<Telerik::WinControls::Primitives::TextPrimitive^>(((Telerik::WinControls::UI::RadButton^)sender)->GetChildAt(0)->GetChildAt(1)->GetChildAt(1)))->ForeColor = System::Drawing::Color::LightGray;

				 this->button_shop_now->Cursor = System::Windows::Forms::Cursors::Default;
	}
	};
}
