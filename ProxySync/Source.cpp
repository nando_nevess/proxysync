#pragma once
#include "SoftwareManager.h"
#include "ProcessManager.h"
#include "Utility.h"
#include <ios>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

void term_func() {
	std::cout << "term_func was called by terminate." << std::endl;
	exit(-1);
}

int main() {
	try {
		set_terminate(term_func);
		SoftwareManager::get();
	}
	catch (...) {
	}
	return 0;
}