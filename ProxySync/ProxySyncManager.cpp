#pragma once
#include "ProxySyncManager.h"
#include "SoftwareManager.h"
#include "WebManager.h"
#include <windows.h>

ProxySyncManager::ProxySyncManager(){
}

void ProxySyncManager::call_send_insert_click_count(){
	int current_execute_id = UserInfoManager::get()->get_current_execute_id_form();

	WebManager::get()->insert_click_count(current_execute_id);
}