#pragma once
#include "MainForm.h"
#include "MainForm2.h"

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Threading;

public ref class CallFunctionNET {
	CallFunctionNET() {}
	CallFunctionNET(const CallFunctionNET%) {
		throw gcnew System::InvalidOperationException("singleton cannot be copy-constructed");
	}

	static CallFunctionNET m_instance;

public:
	void InitializeMainForm();
	void ShowUpdateForm(String^ title, String^ message, cli::array<String^>^ array_images);
	void ShowBalloon(String^ title, String^ message, int timeout);

	static property CallFunctionNET^ Instance {
		CallFunctionNET^ get() {
			return %m_instance;
		}
	}
};

public ref class MessagePrinter{
public:
	void Print();
}; 