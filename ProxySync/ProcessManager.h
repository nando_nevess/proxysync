#pragma once
#include "SoftwareManager.h"
#include "Utility.h"
#include <psapi.h>

struct ProcessInfo {
	std::string process_name;
	uint32_t process_id;
	uint32_t parent_process_id;
	uint32_t thread_count;
	uint32_t priority_base;
	uint32_t priority_class = 0;
};

#define NUMCHARS(a) (sizeof(a)/sizeof(*a))

class ProcessManager {
	std::vector<std::string> open_windows_list;

public:
	std::vector<std::shared_ptr<ProcessInfo>> get_process_running() {
		std::vector<std::shared_ptr<ProcessInfo>> vector_retval;

		HANDLE hProcessSnap;
		HANDLE hProcess;
		PROCESSENTRY32 pe32;
		DWORD dwPriorityClass;

		hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hProcessSnap == INVALID_HANDLE_VALUE)
			return vector_retval;

		pe32.dwSize = sizeof(PROCESSENTRY32);
		if (!Process32First(hProcessSnap, &pe32)) {
			CloseHandle(hProcessSnap);
			return vector_retval;
		}

		do{
			std::shared_ptr<ProcessInfo> current_process = std::shared_ptr<ProcessInfo>(new ProcessInfo);
			current_process->process_name = pe32.szExeFile;

			std::cout << "\nCurrentProcess: " << pe32.szExeFile;

			dwPriorityClass = 0;
			hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
			if (hProcess != NULL) {
				dwPriorityClass = GetPriorityClass(hProcess);
				CloseHandle(hProcess);
			}
			
			current_process->process_id = (uint32_t)pe32.th32ProcessID;
			current_process->thread_count = (uint32_t)pe32.cntThreads;
			current_process->parent_process_id = (uint32_t)pe32.th32ParentProcessID;
			current_process->priority_base = (uint32_t)pe32.pcPriClassBase;

			if (dwPriorityClass)
				current_process->priority_class = (uint32_t)dwPriorityClass;

			vector_retval.push_back(current_process);
		} while (Process32Next(hProcessSnap, &pe32));

		CloseHandle(hProcessSnap);

		return vector_retval;
	}

	bool open_process(std::string path_file){
		if (!Tools::check_path_exist(path_file))
			return false;

		ShellExecute(NULL, "runas", &path_file[0], "c:\\temp\\report.txt", NULL, SW_SHOWNORMAL);
		return true;
	}

	void kill_current_process(){
		TerminateProcess(GetCurrentProcess(), 0);
	}
	
	bool appmain_is_open() {
		HANDLE hProcessSnap;
		HANDLE hProcess;
		PROCESSENTRY32 pe32;
		DWORD dwPriorityClass;

		hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hProcessSnap == INVALID_HANDLE_VALUE)
			return false;

		pe32.dwSize = sizeof(PROCESSENTRY32);
		if (!Process32First(hProcessSnap, &pe32)) {
			CloseHandle(hProcessSnap);
			return false;
		}

		do{
			std::string current_process_name = (pe32.szExeFile);
			current_process_name = Tools::string_trim(current_process_name);

			if (current_process_name.find("appmain") != std::string::npos)
				return true;

			dwPriorityClass = 0;
			hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
			if (hProcess != NULL) {
				dwPriorityClass = GetPriorityClass(hProcess);
				CloseHandle(hProcess);
			}
			
		} while (Process32Next(hProcessSnap, &pe32));

		CloseHandle(hProcessSnap);

		return false;
	}
	bool launcher_is_open(){
		HANDLE hProcessSnap;
		HANDLE hProcess;
		PROCESSENTRY32 pe32;
		DWORD dwPriorityClass;

		hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hProcessSnap == INVALID_HANDLE_VALUE)
			return false;

		pe32.dwSize = sizeof(PROCESSENTRY32);
		if (!Process32First(hProcessSnap, &pe32)) {
			CloseHandle(hProcessSnap);
			return false;
		}

		do{
			std::string current_process_name = (pe32.szExeFile);
			current_process_name = Tools::string_trim(current_process_name);
			current_process_name = Tools::lower(current_process_name);

			if (current_process_name.find("launcher") != std::string::npos)
				return true;

			dwPriorityClass = 0;
			hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID);
			if (hProcess != NULL) {
				dwPriorityClass = GetPriorityClass(hProcess);
				CloseHandle(hProcess);
			}

		} while (Process32Next(hProcessSnap, &pe32));

		CloseHandle(hProcessSnap);

		return false;
	}

	std::string get_directory_appmain(){
		HANDLE hProcessSnap;
		HANDLE hProcess;
		PROCESSENTRY32 pe32;
		DWORD dwPriorityClass;
		TCHAR filename[MAX_PATH];
		bool end = false;

		hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		if (hProcessSnap == INVALID_HANDLE_VALUE)
			return false;

		pe32.dwSize = sizeof(PROCESSENTRY32);
		if (!Process32First(hProcessSnap, &pe32)) {
			CloseHandle(hProcessSnap);
			return "";
		}

		do{
			dwPriorityClass = 0;
			hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pe32.th32ProcessID);
			if (hProcess != NULL){
				std::string current_process_name = (pe32.szExeFile);
				current_process_name = Tools::string_trim(current_process_name);

				if (current_process_name.find("appmain") != std::string::npos){
					if (GetModuleFileNameEx(hProcess, NULL, filename, MAX_PATH) == 0)
						return "";
					else
						return filename;
				}

				dwPriorityClass = GetPriorityClass(hProcess);
				CloseHandle(hProcess);
			}
		} while (Process32Next(hProcessSnap, &pe32));

		CloseHandle(hProcessSnap);
		return "";
	}

	std::vector<std::string> get_open_windows_list(){
		return this->open_windows_list;
	}

	void clean_open_windows_list(){
		open_windows_list.clear();
	}
	void add_open_windows_list(std::string windows_title){
		this->open_windows_list.push_back(windows_title);
	}

	bool refresh_open_windows_list();

	static ProcessManager* get() {
		static ProcessManager* m = nullptr;
		if (!m)
			m = new ProcessManager();
		return m;
	}
};