﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;

namespace AutoUpdate{
    public enum update_state{
        update_state_none,
        update_state_running,
        update_state_fail,
        update_state_timeout,
        update_state_eof,
        update_state_unparsable,
        update_state_success
    }

    public class AutoUpdate{
        public delegate void delegate_on_file_change(string file_name);
        public delegate void delegate_on_download_state(bool state);
        public delegate void delegate_on_percent_change(int percent);
        public delegate void delegate_on_finished();
        public delegate void delegate_on_change_step(string desc);

        public delegate_on_file_change on_file_change;
        public delegate_on_download_state on_download_state;
        public delegate_on_percent_change on_percent_change;
        public delegate_on_finished on_finished;
        public delegate_on_change_step on_change_step;

        Object obj_mutex = new Object();
        string current_string;
        private update_state last_result = update_state.update_state_none;
        private update_state final_result = update_state.update_state_none;
        private Exception last_exception = null;

        private Dictionary<string, bool> servers = new Dictionary<string, bool>();
        private HttpClient net_client = new HttpClient();
        private string sub_url;
        
        private string current_state;
        private string current_file;
        private bool current_downloading = false;
        private int current_percent = 0;

        public AutoUpdate(){
            EraseAllTempFiles();
            SetTimeout(10);
        }

        void SetCurrentFileName(string name){
            on_file_change.Invoke(name);
            current_file = name;
        }
        void SetCurrentPercentage(int percent){
            on_percent_change.Invoke(percent);
            current_percent = percent;
        }
        void SetDownloadingState(bool state){
            on_download_state.Invoke(state);
            current_downloading = state;
        }
        public void SetSubServerUrl(string sub){
            sub_url = sub;
        }
        public void SetTimeout(int timeout){
            net_client.Timeout = new TimeSpan(0, 0, timeout);
        }
        private void SetCurrentString(string _in){
            Monitor.Enter(obj_mutex);
            current_string = _in;
            Monitor.Exit(obj_mutex);
        }

        void NotifyFinished(){
            on_finished.Invoke();
        }

        public string GetCurrentString(){
            var retval = (string)null;

            Monitor.Enter(obj_mutex);
            retval = current_string;
            Monitor.Exit(obj_mutex);

            return retval;
        }
        private string GetAvaliableServer(){
            var resting = servers.Where(v => v.Value == false);
            if (resting.Count() == 0)
                return null;

            Random rand = new Random();
            var res = resting.ElementAt(rand.Next() % resting.Count());
            servers[res.Key] = true;
            return res.Key;
        }
        private string GetBufferMD5(byte[] buffer_){
            try{
                using (var md5 = MD5.Create()){
                    using (var stream = new MemoryStream(buffer_)){
                        return HexStringFromBytes(md5.ComputeHash(stream));
                    }
                }
            }
            catch (Exception ex){
                last_exception = ex;
                return null;
            }
        }
        private string GetFileMd5(string filename){
            try{
                using (var md5 = MD5.Create()){
                    using (var stream = File.OpenRead(filename)){
                        return HexStringFromBytes(md5.ComputeHash(stream));
                    }
                }
            }
            catch (Exception ex){
                last_exception = ex;
                return null;
            }
        }

        private string HexStringFromBytes(byte[] ba){
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);

            return hex.ToString();
        }

        private static List<string> GetAllTempFiles(){
            var current_dir = Directory.GetCurrentDirectory();
            var files = Directory.GetFiles(current_dir);
            return files.Where(file =>
            {
                if (file.ToLower().IndexOf("updatetemp") != -1)
                    return true;
                else
                    return false;
            }).ToList();
        }
        private static string GetFreeTempFileName(){
            var files = GetAllTempFiles();
            int index = 0;
            do{
                Thread.Sleep(300);

                string file_name = "updatetemp" + index;
                if (files.Where(file => file.ToLower().IndexOf(file_name) != -1).Count() == 0)
                    return file_name;

                index++;
            }
            while (true);
        }
        public Exception GetLastException(){
            return last_exception;
        }

        private void EraseAllTempFiles(){
            var files = GetAllTempFiles();
            foreach (var file in files){
                try{
                    File.Delete(file);
                }
                catch (Exception ex){
                    last_exception = ex;
                }
            }
        }

        private bool MoveFileToTemp(string path)
        {
            var new_name = GetFreeTempFileName();
            try{
                File.Move(Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + path,
                    Directory.GetCurrentDirectory() + System.IO.Path.DirectorySeparatorChar + new_name);
            }
            catch (Exception ex){
                last_exception = ex;
                return false;
            }
            return true;
        }

        private bool UpdateFileWithContent(string filename, byte[] content){
            Console.WriteLine("- - - - - START FUNCTION: UpdateFileWithContent(" + filename + ", " + content + ")");
            try{
                if (!RemoveFile(filename)){
                    if (!MoveFileToTemp(filename))
                    {
                        Console.WriteLine("if (!MoveFileToTemp(filename)) || return false;");
                        Console.WriteLine("- - - - - END FUNCTION");
                        return false;
                    }
                }

                var dir = Path.GetDirectoryName(filename);
                if (dir != "" && !Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                Console.WriteLine("0 - File.WriteAllBytes(filename, content)");
                File.WriteAllBytes(filename, content);
            }
            catch (Exception ex){
                last_exception = ex;

                Console.WriteLine("catch (Exception ex) || return false;");
                Console.WriteLine("- - - - - END FUNCTION");
                return false;
            }

            Console.WriteLine("- - - - - END FUNCTION");
            return true;
        }

        private bool RemoveFile(string file){
            if (File.Exists(file)){
                try{
                    File.Delete(file);
                }
                catch (Exception ex){
                    last_exception = ex;
                    return false;
                }
            }
            return true;
        }

        void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e){
            double bytesIn = double.Parse(e.BytesReceived.ToString());
            double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
            double percentage = bytesIn / totalBytes * 100;

            SetCurrentPercentage((int)Math.Truncate(percentage));
            Console.WriteLine("Percent: " + int.Parse(Math.Truncate(percentage).ToString()));
            //progressBar1.Value = int.Parse(Math.Truncate(percentage).ToString());
        }

        void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e){
            Console.WriteLine("Download Completed");
        }

        private bool DownloadFile(string url, string directory){
            Console.WriteLine("\n\n- - - - - START FUNCTION: DownloadFile()");

            Console.WriteLine("directory: " + directory);

            current_percent = 0;
            current_downloading = true;

            WebClient client = new WebClient();

            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProgressChanged);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);
            client.DownloadFileAsync((new System.UriBuilder(url)).Uri, directory);

            while (client.IsBusy)
                Thread.Sleep(100);

            Console.WriteLine("- - - - - END - DownloadFile() || return true");
            return true;
        }

        private bool UpdateFile(string file_path, string hash, string download_url){
            Console.WriteLine("\n\n- - - - - START FUNCTION: UpdateFile()");

            Console.WriteLine("file_path: " + file_path);
            Console.WriteLine("download_url " + download_url);

            string temp_dir = Path.GetDirectoryName(file_path) + "\\tempbuffer";
            if (DownloadFile(download_url, temp_dir) == false){
                Console.WriteLine("if (DownloadFile(download_url) == false) || return false;");
                return false;
            }

            var result = File.ReadAllBytes(temp_dir);
            var downloaded_hash = GetBufferMD5(result);

            Console.WriteLine("downloaded_hash: " + downloaded_hash);
            Console.WriteLine("hash           : " + hash);

            if (hash.ToLower().CompareTo(downloaded_hash.ToLower()) != 0){
                Console.WriteLine("if (hash.ToLower().CompareTo(downloaded_hash.ToLower()) != 0) || return false;");
                return false;
            }

            Console.WriteLine("- - - - - END FUNCTION");
            return UpdateFileWithContent(file_path, result);
        }

        private bool TryOne(string base_url)
        {
            if (base_url == null)
                return false;

            try{
                var process = Process.GetCurrentProcess(); // Or whatever method you are using
                string process_full_path = process.MainModule.FileName;

                Console.WriteLine("Trying to get update xml.");
                //on_change_step.Invoke("Trying to get update xml.");
                var result = net_client.GetStringAsync(base_url + sub_url);

                result.Wait();
                
                Console.WriteLine("Trying to get update xml.");
                if (result != null){
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(result.Result);

                    var file_hashes = doc.FirstChild.ChildNodes;
                    for (int iv = 0; iv < 2; iv++){
                        foreach (var fhash in file_hashes){
                            var el = fhash as XmlElement;
                            var name = el.GetAttribute("name");
                            var path = el.GetAttribute("path");
                            if (name == null || path == null)
                                continue;

                            var full_path = Path.GetDirectoryName(process_full_path) + "\\" + name;
                            
                            if (iv == 0){
                                var fullname = Path.GetDirectoryName(process_full_path) + name;
                                
                                Console.WriteLine("\n\nfullname: " + fullname);
                                Console.WriteLine("process_full_path: " + process_full_path);

                                if (fullname.CompareTo(process_full_path) != 0){
                                    Console.WriteLine("if (fullname.CompareTo(process_full_path) != 0) || continue");
                                    continue;
                                }
                            }

                            SetCurrentFileName(name);

                            var hash = el.GetAttribute("hash");
                            if (path == "")
                                path = ".\\";

                            Console.WriteLine("\n\nfull_path: " + full_path);

                            var local_md5 = GetFileMd5(full_path);

                            Console.WriteLine("local_md5: " + local_md5);
                            Console.WriteLine("hash     : " + hash);

                            if (local_md5 == null || local_md5.ToLower().CompareTo(hash.ToLower()) != 0){
                                Console.WriteLine("if (local_md5 == null || local_md5.ToLower().CompareTo(hash.ToLower()) != 0) || Entrou");
                                bool download_succed = false;
                                for (int i = 0; i < 3; i++){
                                    var link = el.GetAttribute("link");

                                    SetDownloadingState(true);
                                    download_succed = UpdateFile(full_path, hash, link);
                                    SetDownloadingState(false);

                                    if (download_succed){
                                        Console.WriteLine("download_succed || Break");
                                        break;
                                    }
                                }

                                if (!download_succed){
                                    Console.WriteLine(" if (!download_succed) || return false");
                                    return false;
                                }
                                else
                                {
                                    if (iv == 0){
                                        Console.WriteLine("if (iv == 0)|| Restart()");
                                        Restart();
                                    }
                                }
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex){
                last_exception = ex;
                return false;
            }

            return true;
        }

        private void Restart(){
            Process.Start(Process.GetCurrentProcess().MainModule.FileName, "delay=restartwait=3000");
            Process.GetCurrentProcess().Kill();
        }

        public update_state Run(){
            string base_url = null;
            do{
                base_url = GetAvaliableServer();

                Console.WriteLine("\n\n\n\n\nbefore call function tryOne");
                Console.WriteLine("base_url: " + base_url);

                Console.WriteLine("0 - TryOne");
                var result = TryOne(base_url);
                if (result == true){
                    NotifyFinished();
                    return update_state.update_state_success;
                }
                Thread.Sleep(300);
            } while (base_url != null);

            NotifyFinished();

            return update_state.update_state_fail;
        }

        public bool RunAsync(){
            if (final_result == update_state.update_state_running)
                return false;

            (new System.Threading.Thread(new System.Threading.ThreadStart(() => {
                Console.WriteLine("0 - Run");
                final_result = Run(); 
            }))).Start();

            return true;
        }

        public void AddBaseServer(string url){
            servers[url] = false;
        }

        public void RemoveAllServer(){
            servers.Clear();
        }

        update_state WaitResult(){
            while (update_state.update_state_running == final_result)
                Thread.Sleep(300);

            return final_result;
        }
    }
}

